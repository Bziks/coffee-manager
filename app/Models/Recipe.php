<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Таблица блюд
class Recipe extends Model
{
    // Отключаем стандартные два поля,
    // которые создает Laravel: created_at, updated_at
    public $timestamps = false;
}
