<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Связывающая таблица пользователей и заведений
class User_Institution extends Model
{
    // Указываем название таблицы для данной модели.
    // Если не указать, то Laravel посчитает что
    // таблица называется так же как и данный класс: User_Institution
    protected $table = 'user_institution';
    // Отключаем стандартные два поля,
    // которые создает Laravel: created_at, updated_at
    public $timestamps = false;
    
    // Выбираем из БД список заведений
    // которые есть у пользователя $user_id
    public static function getUserInstitutions($user_id)
    {
        /* 
         * Данный код аналогичен запросу:
            SELECT *
            FROM `user_institution`
            LEFT JOIN `institutions`
                ON `user_institution`.`institution_id` = `institutions`.`id`
            WHERE `user_id` = '$user_id'
         */
        return self::where('user_id', $user_id)->leftJoin('institutions', 'user_institution.institution_id', 'institutions.id');
    }
}
