<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Связывающая таблица "особенностей" и заведений
class ServiceTag_InstitutionTag extends Model
{
    // Указываем название таблицы для данной модели.
    // Если не указать, то Laravel посчитает что
    // таблица называется так же как и данный класс: ServiceTag_InstitutionTag
    protected $table = 'institution_tags';
    // Отключаем стандартные два поля,
    // которые создает Laravel: created_at, updated_at
    public $timestamps = false;
    
    // Выбираем из БД список "особенностей" для
    // заведения с id = $institution_id
    public static function getInstitutionTags($institution_id)
    {
        /* 
         * Данный код аналогичен запросу:
            SELECT *
            FROM `institution_tags`
            LEFT JOIN `service_tags`
                ON `institution_tags`.`tag_id` = `service_tags`.`id`
            WHERE `institution_id` = '$institution_id'
         */
        return self::where('institution_id', $institution_id)->leftJoin('service_tags', 'institution_tags.tag_id', 'service_tags.id');
    }
}
