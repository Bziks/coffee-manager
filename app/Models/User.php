<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

// Таблица пользователей
class User extends Authenticatable
{
    use Notifiable;

    /**
     * Поля, которые можно заполнить
     * https://laravel.ru/docs/v5/eloquent#массовое
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * Поля, которые будут скрыты в модели
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
