<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Таблица категорий меню
class CategoryMenu extends Model
{
    // Отключаем стандартные два поля,
    // которые создает Laravel: created_at, updated_at
    public $timestamps = false;
    
    // Указываем название таблицы для данной модели.
    protected $table = 'category_menu';
}
