<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User_Institution;

// Таблица меню для заведений
class InstitutionMenu extends Model
{
    // Отключаем стандартные два поля,
    // которые создает Laravel: created_at, updated_at
    public $timestamps = false;
    
    // Указываем название таблицы для данной модели.
    protected $table = 'institution_menu';
    
    // Выбираем из БД список заведений
    // которые есть у пользователя $user_id
    public static function getInstitutionCategories($userId, $institutionId)
    {
        $institution = User_Institution::getUserInstitutions($userId)->where('institution_id', $institutionId)->get();
        if ($institution) {
            /* 
            * Данный код аналогичен запросу:
               SELECT *
               FROM `institution_menu`
               LEFT JOIN `category_menu`
                   ON `institution_menu`.`institution_id` = `category_menu`.`id`
               WHERE `institution_menu`.`institution_id` = '$institutionId'
            */

           return self::where('institution_id', $institutionId)->leftJoin('category_menu', 'institution_menu.category_id', 'category_menu.id');
        } else {
            return self::where('id', '-1');
        }
    }
    
    // Выбрать блюда привязанные к пользователю,
    // заведению и категории
    public static function getInstitutionCategoryRecipes($userId, $institutionId, $categoryId)
    {
        $institution = User_Institution::getUserInstitutions($userId)->where('institution_id', $institutionId)->get();
        if ($institution) {
            /* 
            * Данный код аналогичен запросу:
               SELECT *
               FROM `institution_menu`
               LEFT JOIN `institutions`
                   ON `institution_menu`.`recipe_id` = `recipes`.`id`
               WHERE `institution_menu`.`institution_id` = '$institutionId' 
                   AND `institution_menu`.`category_id` = '$categoryId'
                   AND `institution_menu`.`recipe_id` IS NOT NULL
            */

           return self::where('institution_id', $institutionId)->where('category_id', $categoryId)->whereNotNull('recipe_id')->leftJoin('recipes', 'institution_menu.recipe_id', 'recipes.id');
        } else {
            return self::where('id', '-1');
        }
    }
    
    // Выбрать блюда привязанные к пользователю и заведению
    public static function getInstitutionRecipes($userId, $institutionId)
    {
        $institution = User_Institution::getUserInstitutions($userId)->where('institution_id', $institutionId)->get();
        if ($institution) {
            /* 
            * Данный код аналогичен запросу:
               SELECT *
               FROM `institution_menu`
               LEFT JOIN `institutions`
                   ON `institution_menu`.`recipe_id` = `recipes`.`id`
               WHERE `institution_menu`.`institution_id` = '$institutionId'
                   AND `institution_menu`.`recipe_id` IS NOT NULL
            */

           return self::where('institution_id', $institutionId)->whereNotNull('recipe_id')->leftJoin('recipes', 'institution_menu.recipe_id', 'recipes.id');
        } else {
            return self::where('id', '-1');
        }
    }
}
