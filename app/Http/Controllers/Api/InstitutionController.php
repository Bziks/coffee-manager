<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Institution;

class InstitutionController extends Controller
{
    //
    public function searchInstitutions(Request $request)
    {
        $city = $request->input('city');
        $street = $request->input('street');
        $json = ['error' => ''];

        if (!empty($city)) {
            $institutions = Institution::where('address', 'like', "%{$city}%,%{$street}%")->get(['id', 'name', 'picture', 'address as addressInfo']);
            $json['institutions'] = $institutions;
        } else {
            $json['error'] = 'Params not found.';
        }

        return response()->json($json);
    }
}
