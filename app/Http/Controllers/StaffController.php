<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User_Institution;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DataTable;

class StaffController extends Controller
{
    /* Когда система использует какой либо class,
     * она сначала вызывает метод __construct
     */
    public function __construct()
    {
        // Указываем что в данный котнролер могут попасть только
        // те пользователи, которые залогинились
        $this->middleware('auth');
        
        $this->middleware('isOwner');
    }
    
    // Просмотреть сотрудников
    public function index(Request $request)
    {
        $institutions = User_Institution::getUserInstitutions(Auth::id())->get();
                
        return view('staff.index', ['institutions' => $institutions]);
    }
    
    // ajax данные о сотрудниках (для плагина DataTable)
    public function staffAjax(Request $request)
    {
        // Выбрать сотрудников, которые привязаны к аккаунту
        $prepareQuery = User::where('owner_id', Auth::id());
        
        /* Настройка столбцов таблицы.
         * Перечислено какие у таблицы столбцы (name)
         * их порядковый индекс (index)
         * и возможно ли по ним делать общий поиск (global_searchable)
         * или поиск только по столбцу (searchable)
         */
        $columns = [
            ['name' => 'name', 'index' => 0, 'global_searchable' => true],
            ['name' => 'email', 'index' => 1, 'global_searchable' => true],
            ['name' => 'institutions', 'index' => 2]
        ];
        
        // Получение сотрудников с учетом фильтров для плагина DataTable
        $data = DataTable::getData($request->all(), $prepareQuery, $columns);
        
        // ПРойтись по всем сотрудникам, которые выводятся на данной странице
        foreach ($data['data'] as $index => $staff) {
            $institutions = [];
            $institutions_id = [];
            // Узнать какие заведения привязаны к ним
            $db_institutions = User_Institution::getUserInstitutions($staff['id'])->get(['name', 'id']);
            foreach ($db_institutions as $institution) {
                // Запомнить названия и id заведений
                $institutions[] = $institution['name'];
                $institutions_id[] = $institution['id'];
            }
            // Добавить названия в виде строки разделённой запятой
            // и добавить id в виде массива в данные таблицы DataTables
            $data['data'][$index]['institutions'] = implode(", ", $institutions);
            $data['data'][$index]['institutions_id'] = $institutions_id;
        }
        
        return $data;
    }
    
    // Создание/редактирование сотрудника
    public function staffEdit(Request $request)
    {
        // Проверка на заполненность полей
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|max:50',
            'institutions' => 'required',
            'isChangePassword' => '',
            'staffId' => 'integer'
        ]);
        
        if ($request->input('staffId') == 0) {
            $userExist = User::where('email', $request->input('email'))->first();

            if ($userExist == null) {
                $user = User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password'))
                ]);
                $user->owner_id = Auth::id();
                $user->save();

                foreach ($request->input('institutions') as $institutionId) {
                    // Создаем новую связь между заведением и пользователем
                    $relationship = new User_Institution();
                    $relationship->user_id = $user->id;
                    $relationship->institution_id = $institutionId;
                    $relationship->save();
                }

                // send email with password
                $params = [
                    'name' => $user->name, 
                    'password' => $request->input('password'),
                    'domain' => $request->server('HTTP_HOST')
                ];
                $email = $user->email;
                Mail::send('emails.newstaff', $params, function ($message) use ($email)
                {
                    $message->to($email);
                });

                return redirect('staff')->with('message', 'Пользователь успешно добавлен.');
            } else {
                return redirect('staff')->with('error', 'Пользователь с таким email уже существует.');
            }
        } else {
            $userExist = User::where('id', $request->input('staffId'))->where('owner_id', Auth::id())->first();
            
            if ($userExist != null) {
                $userExist->name = $request->input('name');
                $userExist->email = $request->input('email');
                
                if ($request->input('isChangePassword') == "on") {
                    $userExist->password = bcrypt($request->input('password'));
                }
                
                $userExist->save();
                
                User_Institution::where('user_id', $userExist->id)->delete();

                foreach ($request->input('institutions') as $institutionId) {
                    // Создаем новую связь между заведением и пользователем
                    $relationship = new User_Institution();
                    $relationship->user_id = $userExist->id;
                    $relationship->institution_id = $institutionId;
                    $relationship->save();
                }

                if ($request->input('isChangePassword') == "on") {
                    // send email with password
                    $params = [
                        'name' => $userExist->name, 
                        'password' => $request->input('password'),
                        'domain' => $request->server('HTTP_HOST')
                    ];
                    $email = $userExist->email;
                    Mail::send('emails.newstaff', $params, function ($message) use ($email)
                    {
                        $message->to($email);
                    });
                }

                return redirect('staff')->with('message', 'Пользователь успешно обновлён.');
            } else {
                return redirect('staff')->with('error', 'Пользователь не найден.');
            }
        }
    }
    
    // Удаление сотрудника
    public function staffRemoveAjax(Request $request)
    {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'id' => 'required|integer'
        ]);
        
        $id = $request->get('id');
        // Выбрать сотрудника
        $userExist = User::where('id', $id)->where('owner_id', Auth::id())->first();

        // Если сотрудник существует, то удалить его
        if ($userExist != null) {
            try {
                User_Institution::where('user_id', $userExist->id)->delete();
                $userExist->delete();

                $json['success'] = true;
            } catch (Exception $ex) {
                $json['error'] = $ex->getMessage();
            }

        } else {
            $json['error'] = 'Сотрудник уже был удален.';
        }
        
        return response()->json($json);
    }
}
