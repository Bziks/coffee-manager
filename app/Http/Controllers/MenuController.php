<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Models\CategoryMenu;
use App\Models\InstitutionMenu;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DataTable;

class MenuController extends Controller
{
    /* Когда система использует какой либо class,
     * она сначала вызывает метод __construct
     */
    public function __construct()
    {
        // Указываем что в данный котнролер могут попасть только
        // те пользователи, которые залогинились
        $this->middleware('auth');
        
        $this->middleware('isOwner');
    }

    // Просмотреть блюда
    public function recipe()
    {
        // Отобразить шаблон menu/recipe
        return view('menu.recipe');
    }
    
    // ajax данные о блюдах (для плагина DataTable)
    public function recipeAjax(Request $request)
    {
        // Выбрать блюда, которые привязаны к аккаунту
        $prepareQuery = Recipe::where('user_id', Auth::id());
        
        /* Настройка столбцов таблицы.
         * Перечислено какие у таблицы столбцы (name)
         * их порядковый индекс (index)
         * и возможно ли по ним делать общий поиск (global_searchable)
         * или поиск только по столбцу (searchable)
         */
        $columns = [
            ['name' => 'name', 'index' => 0, 'global_searchable' => true],
            ['name' => 'weight_type', 'index' => 1],
            ['name' => 'weight_count', 'index' => 2],
            ['name' => 'price', 'index' => 3]
        ];
        
        // Получение блюд с учетом фильтров для плагина DataTable
        return DataTable::getData($request->all(), $prepareQuery, $columns);
    }
    
    // Создание/сохранение блюда
    public function recipeCreate(Request $request) {
        // Проверка на заполненность полей
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'weight_type' => 'required|max:50',
            'weight_count' => 'required|numeric',
            'price' => 'required|numeric',
            'recipeId' => 'integer'
        ]);
        
        $recipeId = $request->input('recipeId');
        
        // Есть ли блюдо с таким названием
        $recipe = Recipe::where('user_id', Auth::id())->where('name', $request->input('name'))->first();

        // Если нет, то создаем/сохраняем
        if ($recipe == null) {
            // Путь куда сохранять картинку блюда
            $path = base_path() . '/public/upload/recipes/';
            // Переменная состояния для сохранения картинки блюда.
            $isImageUpload = true;
            
            // Если блюда нет, то создать
            if ($recipeId == null) {
                $recipe = new Recipe();
                $recipe->user_id = Auth::id();
            } else {
                // Иначе выбрать блюдо по id и редактировать его
                $recipe = Recipe::where('user_id', Auth::id())->where('id', $recipeId)->first();
                
                // Если картинку не изменяют, то указать переменной
                // что картинку загружать не нужно
                if ($request->file('picture') == null) {
                    $isImageUpload = false;
                }
                
                // Если картинку меняют, то удалить текущую
                if ($isImageUpload) {
                    unlink($path.$recipe->picture);
                }
            }
            
            // Если картинку меняют, то изменить её в БД
            if ($isImageUpload) {
                $recipe->picture = 'empty';
            }
            $recipe->name = $request->input('name');
            $recipe->description = $request->input('description');
            $recipe->weight_type = $request->input('weight_type');
            $recipe->weight_count = $request->input('weight_count');
            $recipe->price = $request->input('price');
            $recipe->save();
            
            // Если картинку меняют, то загрузить её на сервер и сохранить
            if ($isImageUpload) {
                $imageTempName = $request->file('picture')->getPathname();
                $imageName = $recipe->id.".".$request->file('picture')->getClientOriginalExtension();
                $request->file('picture')->move($path , $imageName);

                $recipe->picture = $imageName;
                $recipe->save();
            }
        } else {
            return redirect('menu/recipe')->with('error', 'Такое блюдо уже есть в списке блюд.');
        }
        
        return redirect('menu/recipe');
    }
    
    // Удаление блюда
    public function recipeRemoveAjax(Request $request)
    {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'id' => 'required|integer'
        ]);
        
        $id = $request->get('id');
        // Выбрать блюдо
        $recipe = Recipe::where('user_id', Auth::id())->where('id', $id)->first();

        // Если блюдо существует, то удалить его и из меню тоже
        if ($recipe != null) {
            try {
                InstitutionMenu::where('recipe_id', $recipe->id)->delete();
                $recipe->delete();

                $json['success'] = true;
            } catch (Exception $ex) {
                $json['error'] = $ex->getMessage();
            }

        } else {
            $json['error'] = 'Блюдо уже было удалено.';
        }
        
        return response()->json($json);
    }
    
    // Просмотреть категории
    public function category()
    {
        // Отобразить шаблон menu/recipe
        return view('menu.category');
    }
    
    // ajax данные о категориях для меню (для плагина DataTable)
    public function categoryAjax(Request $request) {
        // Выбрать категории, которые привязаны к аккаунту
        $prepareQuery = CategoryMenu::where('user_id', Auth::id());
        
        /* Настройка столбцов таблицы.
         * Перечислено какие у таблицы столбцы (name)
         * их порядковый индекс (index)
         * и возможно ли по ним делать общий поиск (global_searchable)
         * или поиск только по столбцу (searchable)
         */
        $columns = [
            ['name' => 'name', 'index' => 0, 'global_searchable' => true]
        ];
        
        // Получение категорий с учетом фильтров для плагина DataTable
        return DataTable::getData($request->all(), $prepareQuery, $columns);
    }
    
    // Создание/сохранение категории
    public function categoryCreate(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'recipeId' => 'integer'
        ]);
        
        $categoryId = $request->input('categoryId');
        
        // Есть ли категория с таким названием
        $category = CategoryMenu::where('user_id', Auth::id())->where('name', $request->input('name'))->first();

        // Если нет, то сохраняем
        if ($category == null) {
            // Если нет id категории, то создаем новую
            if ($categoryId == null) {
                $category = new CategoryMenu();
                $category->user_id = Auth::id();
            } else {
                // Если есть id, то выбираем категорию по id и редактируем её
                $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            }
            
            $category->name = $request->input('name');
            $category->save();
        } else {
            return redirect('menu/category')->with('error', 'Такая категория уже есть в списке категорий.');
        }
        
        return redirect('menu/category');
    }
    
    // Удаление категории
    public function categoryRemoveAjax(Request $request)
    {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'id' => 'required|integer'
        ]);
        
        $id = $request->get('id');
        // Выбрать категорию
        $category = CategoryMenu::where('user_id', Auth::id())->where('id', $id)->first();

        // Если категория существует, то удалить её из категорий и из меню
        if ($category != null) {
            try {
                InstitutionMenu::where('category_id', $category->id)->delete();
                $category->delete();

                $json['success'] = true;
            } catch (Exception $ex) {
                $json['error'] = $ex->getMessage();
            }

        } else {
            $json['error'] = 'Категория уже была удалена.';
        }
        
        return response()->json($json);
    }
}
