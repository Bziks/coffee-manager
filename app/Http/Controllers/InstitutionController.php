<?php

namespace App\Http\Controllers;

use Symfony\Component\DomCrawler\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ServiceTag;
use App\Models\User_Institution;
use App\Models\Institution;
use App\Models\ServiceTag_InstitutionTag;
use App\Models\CategoryMenu;
use App\Models\Recipe;
use App\Models\InstitutionMenu;
use App\Models\InstitutionType;
use App\Helpers\DataTable;

class InstitutionController extends Controller
{
    /* Когда система использует какой либо class,
     * она сначала вызывает метод __construct
     */
    public function __construct()
    {
        // Указываем что в данный котнролер могут попасть только
        // те пользователи, которые залогинились
        $this->middleware('auth');
        
        $this->middleware('isOwner');
    }
    
    // Страница "Заведения"
    public function index() {
        // Выбрать все заведения пользователя
        $institutions = User_Institution::getUserInstitutions(Auth::id())->get();
        
        return view('institution.index', ['institutions' => $institutions]);
    }
    
    // Страница добавления нового заведения
    public function add() {
        // Выбрать все "особенности"
        $serviceTags = ServiceTag::all();
        // Список типов заведений
        $institutionTypes = InstitutionType::all();
        
        return view('institution.add', ['action' => 'add', 'success' => false, 'model' => new Institution(), 'serviceTags' => $serviceTags, 'institutionTypes' => $institutionTypes]);
    }
    
    // Сохранить новое заведение
    public function postAdd(Request $request) {
        /* Проверка на заполненность формы, где
         * required - обязательное поля
         * max:N - максимальное количество символов (N) в поле
         */
        $this->validate($request, [
            'name' => 'required|max:100',
            'address' => 'required|max:255',
            'description' => 'required|max:1000',
            'type' => 'required|integer'
        ]);
        
        // Создание нового заведения
        $institution = new Institution();
        $institution->name = $request->input('name');
        $institution->address = $request->input('address');
        $institution->description = $request->input('description');
        $institution->type = $request->input('type');
        $institution->picture = '';
        /* Сохраняет заведение, аналогично запросу
            INSERT INTO `institution`
            (`name`, `address`, `description`, `type`, `picture`)
            VALUES
            ('$_POST['name']', '$_POST['address']', '$_POST['description']', '$_POST['type']', '')
         */
        $institution->save();
        
        // Загрузка файла на сервер
        if ($request->file('picture')) {
            // Название файла
            $imageName = $institution->id.".".$request->file('picture')->getClientOriginalExtension();
            // Путь куда сохранить на сервере
            $path = base_path() . '/public/upload/institutions/';
            // Сохранение
            $request->file('picture')->move($path , $imageName);
            
            // Сохраняем название файла в БД
            $institution->picture = $imageName;
            /* Аналогично запросу
                UPDATE `institution`
                SET `picture` = '$imageName'
                WHERE `id` = '$institution->id'
             */
            $institution->save();
        }
        
        if ($request->input('serviceTag')) {
            foreach ($request->input('serviceTag') as $tag) {
                $institutionTag = new ServiceTag_InstitutionTag();
                $institutionTag->tag_id = $tag;
                $institutionTag->institution_id = $institution->id;
                $institutionTag->save();
            }
        }
        
        // Создаем новую связь между заведением и пользователем
        $relationship = new User_Institution();
        $relationship->user_id = Auth::id();
        $relationship->institution_id = $institution->id;
        $relationship->save();
        
        return view('institution.add', ['action' => 'add', 'success' => true, 'model' => new Institution()]);
    }
    
    // Страница редактирования заведения
    public function edit($id) {
        // Выбрать заведение залогинившегося пользователя у которого id = $id
        $model = User_Institution::getUserInstitutions(Auth::id())->where('id', $id)->first();
        // Выбрать все "особенности"
        $serviceTags = ServiceTag::all();
        
        // Для безопасности: если заведение есть в БД,
        // то выбираем "особенности" этого заведения
        if ($model != null) {
            $institutionServiceTags = ServiceTag_InstitutionTag::getInstitutionTags($id)->get();
            $modelServiceTags = [];
            foreach ($institutionServiceTags as $tag) {
                $modelServiceTags[$tag['id']] = true;
            }
            $model['pictureUpdate'] = strtotime($model['pictureUpdate']);
        }
        
        // Список категорий пользователя
        $categories = CategoryMenu::where('user_id', Auth::id())->get(['id', 'name']);
        // Список блюд пользователя
        $recipes = Recipe::where('user_id', Auth::id())->get(['id', 'name']);
        // Список категорий текущего заведения
        $institutionCategories = InstitutionMenu::getInstitutionCategories(Auth::id(), $id)->where('recipe_id', null)->orderBy('institution_menu.position', 'asc')->get(['category_menu.name', 'category_menu.id', 'institution_menu.position']);
        // Список типов заведений
        $institutionTypes = InstitutionType::all();
        
        return view('institution.add', ['action' => 'edit', 'success' => false, 'model' => $model, 'serviceTags' => $serviceTags, 'modelServiceTags' => $modelServiceTags, 'categories' => $categories, 'institutionCategories' => $institutionCategories, 'institutionTypes' => $institutionTypes, 'recipes' => $recipes]);
    }
    
    // ajax данные о категориях для меню (для плагина DataTable)
    public function categoryAjax(Request $request) {
        $this->validate($request, [
            'institutionId' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        // Выбрать категории, которые привязаны к заведению
        $prepareQuery = InstitutionMenu::getInstitutionCategories(Auth::id(), $institutionId)->where('recipe_id', null);
        
        /* Настройка столбцов таблицы.
         * Перечислено какие у таблицы столбцы (name)
         * их порядковый индекс (index)
         * и возможно ли по ним делать общий поиск (global_searchable)
         * или поиск только по столбцу (searchable)
         */
        $columns = [
            ['name' => 'name', 'index' => 0, 'global_searchable' => true],
            ['name' => 'position', 'index' => 1]
        ];
        
        // Получение категорий с учетом фильтров для плагина DataTable
        return DataTable::getData($request->all(), $prepareQuery, $columns);
    }
    
    // Удалить категорию из заведения и все её блюда
    public function categoryRemoveAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'id' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('id');
        
        // Проверка есть ли данное заведение у пользователя
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            // Проверка есть ли данная категория у пользователя в данном заведении
            $institutionCategory = InstitutionMenu::getInstitutionCategories(Auth::id(), $institutionId)->where('category_id', $categoryId);
            
            try {
                // Удалить категорию
                $institutionCategory->delete();
                $json['success'] = true;
            } catch (Exception $ex) {
                // Показать ошибку
                $json['error'] = $ex->getMessage();
            }
        }
        
        return response()->json($json);
    }
    
    // Добавить категорию к заведению
    public function categoryAddAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'id' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('id');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('id', $categoryId)->first();
            
            if ($category != null) {
                try {
                    $institutionCategory = new InstitutionMenu();
                    $institutionCategory->institution_id = $institution['id'];
                    $institutionCategory->category_id = $category['id'];
                    $institutionCategory->position = InstitutionMenu::where('institution_id', $institution['id'])->whereNull('recipe_id')->count();
                    $institutionCategory->save();
                    $json['success'] = true;
                } catch (Exception $ex) {
                    $json['error'] = $ex->getMessage();
                }
                
            }
        }
        
        return response()->json($json);
    }
    
    // Создать категорию и добавить к заведению
    public function categoryCreateAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'name' => 'required|max:100'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryName = $request->get('name');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('user_id', Auth::id())->where('name', $categoryName)->first();
            
            if ($category == null) {
                try {
                    $newCategory = new CategoryMenu();
                    $newCategory->user_id = Auth::id();
                    $newCategory->name = $categoryName;
                    $newCategory->save();

                    $institutionCategory = new InstitutionMenu();
                    $institutionCategory->institution_id = $institution['id'];
                    $institutionCategory->category_id = $newCategory->id;
                    $institutionCategory->position = InstitutionMenu::where('institution_id', $institution['id'])->count();
                    $institutionCategory->save();
                    
                    $json['success'] = true;
                } catch (Exception $ex) {
                    $json['error'] = $ex->getMessage();
                }
                
            } else {
                $json['error'] = 'Такая категория уже существует.';
            }
        }
        
        return response()->json($json);
    }
    
    // Изменить позицию категории в заведении
    public function categoryEditAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer',
            'position' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('categoryId');
        $position = $request->get('position');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                try {
                    $item = InstitutionMenu::where('institution_id', $institutionId)->where('category_id', $categoryId)->where('recipe_id', null)->first();
                    $item['position'] = $position;
                    $item->save();
                    
                    $json['success'] = true;
                } catch (Exception $ex) {
                    $json['error'] = $ex->getMessage();
                }
                
            } else {
                $json['error'] = 'Категория не найдена.';
            }
        }
        
        return response()->json($json);
    }
    
    // Создать блюдо и добавить в категорию
    public function recipeCreate(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'picture' => 'required',
            'description' => 'required|max:255',
            'weight_type' => 'required|max:50',
            'weight_count' => 'required|numeric',
            'price' => 'required|numeric',
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer'
        ]);
        
        $institutionId = $request->input('institutionId');
        $categoryId = $request->input('categoryId');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                $recipe = new Recipe();
                $recipe->user_id = Auth::id();
                $recipe->picture = 'empty';
                $recipe->name = $request->input('name');
                $recipe->description = $request->input('description');
                $recipe->weight_type = $request->input('weight_type');
                $recipe->weight_count = $request->input('weight_count');
                $recipe->price = $request->input('price');
                $recipe->save();
                
                $imageTempName = $request->file('picture')->getPathname();
                $imageName = $recipe->id.".".$request->file('picture')->getClientOriginalExtension();
                $path = base_path() . '/public/upload/recipes/';
                $request->file('picture')->move($path , $imageName);
                
                $recipe->picture = $imageName;
                $recipe->save();
                
                $institutionCategory = new InstitutionMenu();
                $institutionCategory->institution_id = $institutionId;
                $institutionCategory->category_id = $categoryId;
                $institutionCategory->recipe_id = $recipe->id;
                $institutionCategory->position = 0;
                $institutionCategory->save();
            }
        }
        
        return redirect('institution/edit/'.$institutionId.'#menu')->with('categoryId', $categoryId);
    }
    
    // Добавить блюдо в категорию
    public function recipeAddAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'recipeId' => 'required|integer',
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer'
        ]);
        
        $recipeId = $request->input('recipeId');
        $institutionId = $request->input('institutionId');
        $categoryId = $request->input('categoryId');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                $recipe = Recipe::where('user_id', Auth::id())->first();
                
                if ($recipe) {
                    try {
                        $json['success'] = true;
                        
                        $institutionCategory = new InstitutionMenu();
                        $institutionCategory->institution_id = $institutionId;
                        $institutionCategory->category_id = $categoryId;
                        $institutionCategory->recipe_id = $recipeId;
                        $institutionCategory->position = 0;
                        $institutionCategory->save();
                    } catch (Exception $ex) {
                        $json['error'] = $ex->getMessage();
                    }
                } else {
                    $json['error'] = 'Блюдо не найдено.';
                }
            } else {
                $json['error'] = 'Категория не найдена.';
            }
        } else {
            $json['error'] = 'Заведение не найдено.';
        }
        
        return response()->json($json);
    }
    
    // Список блюд, привязаных к заведению и категории
    public function recipeAjax(Request $request) {
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('categoryId');
        
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                // Выбрать блюда, которые привязаны к заведению и категории
                $prepareQuery = InstitutionMenu::getInstitutionCategoryRecipes(Auth::id(), $institutionId, $categoryId);

                /* Настройка столбцов таблицы.
                 * Перечислено какие у таблицы столбцы (name)
                 * их порядковый индекс (index)
                 * и возможно ли по ним делать общий поиск (global_searchable)
                 * или поиск только по столбцу (searchable)
                 */
                $columns = [
                    ['name' => 'name', 'index' => 0, 'global_searchable' => true],
                    ['name' => 'weight_type', 'index' => 1],
                    ['name' => 'weight_count', 'index' => 2],
                    ['name' => 'price', 'index' => 3]
                ];
                
                // Получение категорий с учетом фильтров для плагина DataTable
                return DataTable::getData($request->all(), $prepareQuery, $columns);
            }
        }
        
        return false;
    }
    
    // Изменить позицию блюда
    public function recipeEditAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer',
            'recipeId' => 'required|integer',
            'position' => 'required|integer'
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('categoryId');
        $recipeId = $request->get('recipeId');
        $position = $request->get('position');
        
        // Проверка есть ли данное заведение у пользователя
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            // Проверка есть ли категория у пользователя
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                // Проверка есть ли блюдо у пользователя
                $recipe = InstitutionMenu::where('institution_id', $institutionId)->where('category_id', $categoryId)->where('recipe_id', $recipeId)->first();
                
                if ($recipe != null) {
                    try {
                        // Новая позиция
                        $recipe->position = $position;
                        $recipe->save();

                        $json['success'] = true;
                    } catch (Exception $ex) {
                        $json['error'] = $ex->getMessage();
                    }
                } else {
                    $json['error'] = 'Блюдо не найдено.';
                }
            } else {
                $json['error'] = 'Категория не найдена.';
            }
        } else {
            $json['error'] = 'Заведение не найдено.';
        }
        
        return response()->json($json);
    }
    
    // Удалить блюдо из категории
    public function recipeRemoveAjax(Request $request) {
        $json = [
            'success' => false,
            'error' => 'not valid'
        ];
        $this->validate($request, [
            'institutionId' => 'required|integer',
            'categoryId' => 'required|integer',
            'recipeId' => 'required|integer',
        ]);
        
        $institutionId = $request->get('institutionId');
        $categoryId = $request->get('categoryId');
        $recipeId = $request->get('recipeId');
        
        // Проверка есть ли данное заведение у пользователя
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $institutionId)->first();
        
        if ($institution != null) {
            // Проверка есть ли категория у пользователя
            $category = CategoryMenu::where('user_id', Auth::id())->where('id', $categoryId)->first();
            
            if ($category != null) {
                // Проверка есть ли блюдо у пользователя
                $recipe = InstitutionMenu::where('institution_id', $institutionId)->where('category_id', $categoryId)->where('recipe_id', $recipeId);
                
                if ($recipe->first() != null) {
                    try {
                        // Удалить блюдо
                        $recipe->delete();

                        $json['success'] = true;
                    } catch (Exception $ex) {
                        $json['error'] = $ex->getMessage();
                    }
                } else {
                    $json['error'] = 'Блюдо не найдено.';
                }
            } else {
                $json['error'] = 'Категория не найдена.';
            }
        } else {
            $json['error'] = 'Заведение не найдено.';
        }
        
        return response()->json($json);
    }
    
    // Сохранить редактируемое заведение
    public function postEdit(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|max:100',
            'address' => 'required|max:255',
            'description' => 'required|max:1000',
            'type' => 'required|integer'
        ]);
        
        // Проверка есть ли данное заведение у пользователя
        $model = User_Institution::getUserInstitutions(Auth::id())->where('id', $id)->first();
        if ($model != null) {
            // Сохранение новой картинки, если её обновили
            $imageName = null;
            if ($request->input('pictureIsChange') === "true") {
                if ($request->file('picture')) {
                    $imageTempName = $request->file('picture')->getPathname();
                    $imageName = $id.".".$request->file('picture')->getClientOriginalExtension();
                    $path = base_path() . '/public/upload/institutions/';
                    $request->file('picture')->move($path , $imageName);
                } else {
                    $imageName = '';
                }
            }
            
            // Выбрать заведение и обновить картинку, если нужно
            $institution = Institution::where('id', $id)->first();
            $institution->name = $request->input('name');
            $institution->address = $request->input('address');
            if ($imageName !== null) {
                $institution->picture = $imageName;
                $institution->pictureUpdate = \DB::raw('CURRENT_TIMESTAMP');
            }
            $institution->description = $request->input('description');
            $institution->type = $request->input('type');
            $institution->save();
            
            // Удаляем все "особенности" заведения и сохраняем новые
            ServiceTag_InstitutionTag::where('institution_id', $id)->delete();
            
            if ($request->input('serviceTag')) {
                foreach ($request->input('serviceTag') as $tag) {
                    $institutionTag = new ServiceTag_InstitutionTag();
                    $institutionTag->tag_id = $tag;
                    $institutionTag->institution_id = $id;
                    $institutionTag->save();
                }
            }
        }
        
        return view('institution.add', ['action' => 'edit', 'success' => true, 'model' => $model]);
    }
    
    // Страница удаления заведения
    public function remove($id) {
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $id)->first();
        
        if ($institution) {
            $model = [
                'id' => $institution['id'],
                'name' => $institution['name']
            ];
            
            return view('institution.remove', ['deleted' => false, 'model' => $model]);
        } else {
            redirect()->action('InstitutionController@index');
        }
    }
    
    // Удалить заведение
    public function postRemove(Request $request, $id) {
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $id)->first();
        
        if ($institution && $request->input('accept')) {
            Institution::where('id', $id)->delete();
            User_Institution::where('institution_id', $id)->delete();
            InstitutionMenu::where('institution_id', $id)->delete();
            
            return view('institution.remove', ['deleted' => true]);
        } else {
            redirect()->action('InstitutionController@index');
        }
    }
    
    // Просмотреть информацию о заведению
    public function view($id) {
        $institution = User_Institution::getUserInstitutions(Auth::id())->where('id', $id)->first();
        
        if ($institution) {
            $serviceTags = ServiceTag::all();
            
            $institutionServiceTags = ServiceTag_InstitutionTag::getInstitutionTags($id)->get();
            $modelServiceTags = [];
            foreach ($institutionServiceTags as $tag) {
                $modelServiceTags[$tag['id']] = true;
            }
            
            $institutionType = InstitutionType::where('id', $institution['type'])->first(['name']);
            $institution['description'] = str_replace("\n", "<br>", $institution['description']);
            
            // Список блюд пользователя
            $recipesDb = InstitutionMenu::getInstitutionRecipes(Auth::id(), $id)->orderBy('institution_menu.position', 'asc')->get([
                'recipes.name',
                'recipes.picture',
                'recipes.description',
                'recipes.weight_type',
                'recipes.weight_count',
                'recipes.price',
                'institution_menu.category_id'
            ]);
            $recipes = [];
            foreach($recipesDb as $recipe) {
                $recipes[$recipe['category_id']][] = $recipe;
            }
            
            // Список категорий текущего заведения
            $institutionCategories = InstitutionMenu::getInstitutionCategories(Auth::id(), $id)->whereNull('recipe_id')->orderBy('institution_menu.position', 'asc')->get(['category_menu.name', 'category_menu.id', 'institution_menu.position']);
            
            return view('institution.view', [
                'model' => $institution,
                'institutionType' => $institutionType['name'],
                'serviceTags' => $serviceTags,
                'modelServiceTags' => $modelServiceTags,
                'institutionCategories' => $institutionCategories,
                'recipes' => $recipes
            ]);
        } else {
            redirect()->action('InstitutionController@index');
        }
    }
}
