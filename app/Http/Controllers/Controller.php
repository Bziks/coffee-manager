<?php

/* namespace - пространство имен, используется для того что бы разделять
 * файлы (class) с одинаковым именем, но которые находятся в разных папках.
 * Название для namespace состоит из названий папок.
 * App/Http/Controllers - где App абстрактная папка всего проекта,
 * Http\Controllers - путь по которому находится файл
 */
namespace App\Http\Controllers;

// use - подключает другие файлы (class) на основе их namespace
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
