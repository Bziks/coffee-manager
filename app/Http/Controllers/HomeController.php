<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institution;

class HomeController extends Controller
{
    // Главная страница сайта
    public function index()
    {
        // Выбрать количество всех заведений
        $countInstitution = Institution::count();
        
        return view('index', ['countInstitution' => $countInstitution]);
    }
    
    // Страница "О нас"
    public function aboutUs()
    {
        return view('home.aboutUs');
    }
    
    // Страница "Контакты"
    public function contacts()
    {
        return view('home.contacts');
    }
}
