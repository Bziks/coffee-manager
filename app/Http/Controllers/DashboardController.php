<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User_Institution;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /* Когда система использует какой либо class,
     * она сначала вызывает метод __construct
     */
    public function __construct()
    {
        // Указываем что в данный котнролер могут попасть только
        // те пользователи, которые залогинились
        $this->middleware('auth');
    }

    // Просмотреть главную страницу Dashboard'a
    public function index()
    {
        // Выбрать все заведения пользователя
        $institutions = User_Institution::getUserInstitutions(Auth::id());
        
        /* Отобразить шаблон dashboard и передать в него модель, где
         * countInstitutions - кол-во заведений у пользователя
         * institutions - последние 3 заведения пользователя
         */
        return view('dashboard', array('countInstitutions' => $institutions->count(), "institutions" => $institutions->orderBy('id', 'DESC')->limit(3)->get()));
    }
}
