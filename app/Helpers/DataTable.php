<?php
/**
 * Описание DataTable
 *
 * Данный класс генерирует такую структуру данных, которая подходит для
 * плагина DataTable. Более подробная информация на сайте
 * https://datatables.net/manual/server-side
 */
namespace App\Helpers;

class DataTable {
    
    /**
     * Генерирует данные для плагина DataTable
     * 
     * $request - POST данные из http запроса
     * $prepareQuery - подготовленные данные для выборки из БД
     * $columns - список столбцов, которые отображаются на странице в таблице
     * 
     * @param Request $request
     * @param Eloquent $prepareQuery
     * @param Array $columns
     * @return Array
     */
    public static function getData($request, $prepareQuery, $columns)
    {
        // Получаем общее кол-во записей в БД
        $total = $prepareQuery->count();
        
        // Фильтрация данных (если происходит поиск)
        $prepareQuery = self::filter($request, $prepareQuery, $columns);
        // Общее кол-во отфильтрованых данных
        $filterTotal = $prepareQuery->count();
        
        // Сортировка
        $prepareQuery = self::order($request, $prepareQuery, $columns);
        // Выборка данных для определенной страницы
        $prepareQuery = self::limit($request, $prepareQuery);
        // Преобразование данных из Eloquent в Array
        $data = $prepareQuery->get();
        
        // Структура данных для DataTable плагина
        $return = [
            'draw' => isset($request['draw']) ? intval($request['draw']) : 0,
            'recordsTotal' => $total,
            'recordsFiltered' => $filterTotal,
            'data' => $data
        ];
        
        return $return;
    }
    
    /**
     * Применить фильтр к данным
     * 
     * Если в таблице вводят символы в строку поиска, то задействуется эта функция
     * 
     * @param Request $request
     * @param Eloquent $prepareQuery
     * @param Array $columns
     * @return Eloquent
     */
    private static function filter($request, $prepareQuery, $columns)
    {
        // Кол-во столбцов в таблице
        $columnLen = count($request['columns']);
        // Выбрать из массива $columns только ключи name
        $dtColumns = self::pluck($columns, 'name');
        
        // Поиск по всем столбцам (если для этих столбцов разрешен глобальный поиск)
        if (isset($request['search']) && $request['search']['value'] != '') {
            // Что именно искать (данные из поля поиска)
            $searchVal = $request['search']['value'];
            
            // Перебираем каждую колонку
            for ($i = 0; $i < $columnLen; $i++) {
                $requestColumn = $request['columns'][$i];
                // Т.к. данные от пользователей можно подделать, поэтому происходит 
                // проверка есть ли такая колонка в массиве $dtColumns, который
                // основан на массиве $columns
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                // Проверка, разрешен ли поиск по данному столбцу
                if ($requestColumn['searchable'] == 'true' && !empty($column['global_searchable']) && $column['global_searchable'] == true) {
                    // Поиск по столбцу, данный код в подготовленный SQL запрос добавляет код:
                    // AND `$column['name']` LIKE '%$searchVal%'
                    $prepareQuery = $prepareQuery->where($column['name'], 'LIKE', "%{$searchVal}%");
                }
            }
        }
        
        // Поиск по отдельной колонке (данный функционал не используется, но вдруг в будущем понадобится)
        // Функционал похож на предыдущий, отличие в том что там по всем колонкам шел поиск, а тут
        // только по одной
        if (isset($request['columns'])) {
            for ($i = 0; $i < $columnLen; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $searchVal = $requestColumn['search']['value'];
                if ($requestColumn['searchable'] == 'true' && !empty($column['searchable']) && $column['searchable'] == true && $searchVal != '') {
                    $prepareQuery = $prepareQuery->where($column['name'], 'LIKE', "%{$searchVal}%");
                }
            }
        }
        
        return $prepareQuery;
    }
    
    /**
     * Сортировка (ORDER BY)
     * 
     * @param Request $request
     * @param Eloquent $prepareQuery
     * @param Array $columns
     * @return Eloquent
     */
    private static function order($request, $prepareQuery, $columns)
    {
        // Если отсортирована любая колонка
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = self::pluck($columns, 'name');
            $orderLen = count($request['order']);
            
            // Проходимся по каждой колонке
            for ($i = 0; $i < $orderLen; $i++) {
                // Узнаем какую колонку сортируют
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                // Если сортировка разрешена для данной колонки
                if ($requestColumn['orderable'] == 'true') {
                    // Сортируем, данный код в подготовленный SQL запрос добавляет код:
                    // ORDER BY `$column['name']` $dir
                    $dir = $request['order'][$i]['dir'] === 'asc' ? 'asc' : 'desc';
                    $prepareQuery = $prepareQuery->orderBy($column['name'], $dir);
                }
            }
        }
        
        return $prepareQuery;
    }
    
    /**
     * Применить ограничение к запросу (LIMIT x,x)
     * 
     * Функция выбирает данные, основываясь на какой странице находится пользователь.
     * Простая пагинация
     * 
     * @param Request $request
     * @param Eloquent $prepareQuery
     * @return Eloquent
     */
    private static function limit($request, $prepareQuery)
    {
        if (isset($request['start']) && $request['length'] != -1) {
            // Данный код в подготовленный SQL запрос добавляет код:
            // LIMIT intval($request['start']), intval($request['length'])
            $prepareQuery = $prepareQuery->skip(intval($request['start']))->take(intval($request['length']));
        }
                
        return $prepareQuery;
    }
    
    /**
     * Выбирает ключ $prop из массива $a и возвращает обычный массив на основе
     * выбраных данных
     *
     *  @param  Array  $a    Входящий массив
     *  @param  String $prop Ключ для чтения
     *  @return Array        
     */
    static function pluck ( $a, $prop )
    {
        $out = array();
        for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }
}