<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Главная страница
Route::get('/', 'HomeController@index');

//Страницы с разной инфой
Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/contacts', 'HomeController@contacts');

// Авторизация/регистрация
Auth::routes();

// Dashboard
Route::get('/dashboard', 'DashboardController@index');

// Заведения
Route::get('/institution', 'InstitutionController@index');
Route::get('/institution/add', 'InstitutionController@add');
Route::post('/institution/add', 'InstitutionController@postAdd');
Route::get('/institution/edit/{id}', 'InstitutionController@edit')->where('id', '[0-9]+');
Route::post('/institution/edit/{id}', 'InstitutionController@postEdit')->where('id', '[0-9]+');
Route::get('/institution/remove/{id}', 'InstitutionController@remove')->where('id', '[0-9]+');
Route::post('/institution/remove/{id}', 'InstitutionController@postRemove')->where('id', '[0-9]+');
Route::get('/institution/{id}', 'InstitutionController@view')->where('id', '[0-9]+');
Route::post('/institution/categoryAjax', 'InstitutionController@categoryAjax');
Route::post('/institution/categoryRemoveAjax', 'InstitutionController@categoryRemoveAjax');
Route::post('/institution/categoryAddAjax', 'InstitutionController@categoryAddAjax');
Route::post('/institution/categoryCreateAjax', 'InstitutionController@categoryCreateAjax');
Route::post('/institution/categoryEditAjax', 'InstitutionController@categoryEditAjax');
Route::post('/institution/recipeCreate', 'InstitutionController@recipeCreate');
Route::post('/institution/recipeAjax', 'InstitutionController@recipeAjax');
Route::post('/institution/recipeAddAjax', 'InstitutionController@recipeAddAjax');
Route::post('/institution/recipeEditAjax', 'InstitutionController@recipeEditAjax');
Route::post('/institution/recipeRemoveAjax', 'InstitutionController@recipeRemoveAjax');

// Меню: Блюда, Категории
Route::get('/menu/recipe', 'MenuController@recipe');
Route::post('/menu/recipeAjax', 'MenuController@recipeAjax');
Route::post('/menu/recipeRemoveAjax', 'MenuController@recipeRemoveAjax');
Route::post('/menu/recipeCreate', 'MenuController@recipeCreate');
Route::get('/menu/category', 'MenuController@category');
Route::post('/menu/categoryAjax', 'MenuController@categoryAjax');
Route::post('/menu/categoryRemoveAjax', 'MenuController@categoryRemoveAjax');
Route::post('/menu/categoryCreate', 'MenuController@categoryCreate');

// Сотрудники
Route::get('/staff', 'StaffController@index');
Route::post('/staff/staffAjax', 'StaffController@staffAjax');
Route::post('/staff/staffRemoveAjax', 'StaffController@staffRemoveAjax');
Route::post('/staff/staffEdit', 'StaffController@staffEdit');