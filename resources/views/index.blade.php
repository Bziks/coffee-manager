@extends('layouts.app')

@section('content')
<!-- 
1. Добавить кодировку (utf-8) - meta charset (Выполнено)
2. Подключить бутстрап - http://getbootstrap.com/getting-started/#download-cdn (Выполнено)
3. Создать верхнее меню: ссылки на авторизацию/регистрацию, или имя если залогинен (navbar) - http://getbootstrap.com/components/#navbar (Выполнено)
4. Создать главный контент со слайдером (container, image slider) (Выполнено)
5. Создать футер (Выполнено)
---------------------------
6. Д/з добавить странички и добавить ссылки на них в футере (Выполнено)
7. Д/з переименовать страницы входа и регистрации на рус. язык (Выполнено)
8. Страница в дэшборде "Заведения", в ней можно будет создавать свои заведения, просматривать список заведений
-->

<div id="home-slider" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#home-slider" data-slide-to="0" class="active"></li>
        <li data-target="#home-slider" data-slide-to="1"></li>
        <li data-target="#home-slider" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="https://pp.vk.me/c638430/v638430381/260b7/kEjEKSklef4.jpg" alt="Картинка 1">
            <div class="carousel-caption">
                Кафе «Домашний уют»
            </div>
        </div>
        <div class="item">
            <img src="http://p1.zoon.ru/preview/9-_6kq-ESMxxuOHRLf8cbg/1920x1080x75/1/d/8/original_50b722b7a0f302a76500001b_56fe46da648fc.jpg" alt="Картинка 2">
            <div class="carousel-caption">
                Ресторан «Апрель»
            </div>

        </div>
        <div class="item">
            <img src="http://p0.zoon.ru/preview/0iqbjRsbQxqbWsZKVYLHOQ/1920x1080x75/0/d/1/original_4f85bd503c72dd8114000f73_5166db010e5b7.jpg" alt="Картинка 3">
            <div class="carousel-caption">
                Ресторан «Старая таверна»
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#home-slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#home-slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<br>
<div class="container">
    <div class="alert alert-warning text-center">
        С нами уже {{ $countInstitution }} заведений.
    </div>
</div>
@endsection