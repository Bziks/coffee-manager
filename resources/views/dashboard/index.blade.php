@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <!-- Левое меню -->
            <div id="sidebar-menu">
                <div class="list-group">
                    <a href="{{ url('/dashboard') }}" class="list-group-item {{ Request::is('dashboard') ? 'active' : '' }}">Dashboard</a>
                    @if (Request::user()->owner_id == null)
                    <a href="{{ url('/institution') }}" class="list-group-item {{ Request::is('institution*') ? 'active' : '' }}">Заведения</a>
                    <a href="#" class="list-group-item {{ Request::is('menu/category') || Request::is('menu/recipe') ? 'active' : '' }}" aria-expanded="{{ Request::is('menu/category') || Request::is('menu/recipe') ? 'true' : 'false' }}" data-toggle="collapse" data-target="#sm1" data-parent="#sidebar-menu">Блюда/Категории</a>
                    <div id="sm1" class="sublinks collapse {{ Request::is('menu/category') || Request::is('menu/recipe') ? 'in' : '' }}" aria-expanded="{{ Request::is('menu/category') || Request::is('menu/recipe') ? 'true' : 'false' }}">
                        <a href="{{ url('/menu/recipe') }}" class="list-group-item small {{ Request::is('menu/recipe') ? 'active' : '' }}">Блюда</a>
                        <a href="{{ url('/menu/category') }}" class="list-group-item small {{ Request::is('menu/category') ? 'active' : '' }}">Категории</a>
                    </div>
                    <a href="{{ url('/staff') }}" class="list-group-item {{ Request::is('staff*') ? 'active' : '' }}">Сотрудники</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="content">
                @yield('dashboard.content')
            </div>
        </div>
    </div>
</div>
@endsection