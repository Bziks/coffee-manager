@extends('dashboard.index')

@section('dashboard.content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/js/DataTables/datatables.min.js') }}"></script>

    <div class="text-right">
        <button class="btn btn-warning" data-toggle="modal" data-target="#create-staff-modal">Добавить сотрудника</button>
    </div>
    <br>
    
    @if (session('message'))
    <div class="alert alert-success text-center">{{ session('message') }}</div>
    @endif
    
    @if (session('error'))
    <div class="alert alert-danger text-center">{{ session('error') }}</div>
    @endif
    
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <table id="staff-table" class="table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ФИО</th>
                <th>Email</th>
                <th>Заведения</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ФИО</th>
                <th>Email</th>
                <th>Заведения</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    
    <script type="text/javascript" src="{{ asset("/js/staff.js") }}"></script>
    <script>
    $(function () {
        staff.initTable();
    });
    </script>
    
    <!-- Modals -->
    <!-- Создание/редактирование категории -->
    <div class="modal fade" id="create-staff-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3"><span class="staff-title-modal"></span> блюда</h4>
                </div>
                <div class="modal-body">
                    <ul class="list-unstyled text-danger bg-danger error-list"></ul>

                    {!! Form::open(array('url' => 'staff/staffEdit')) !!}

                        <div class="form-group">
                            <label for="staff-form-name">ФИО</label>
                            <input type="text" class="form-control" id="staff-form-name" name="name" placeholder="Селезнев Дмитрий Борисович">
                        </div>
                    
                        <div class="form-group">
                            <label for="staff-form-email">Email</label>
                            <input type="text" class="form-control" id="staff-form-email" name="email" placeholder="example@gmail.com">
                        </div>
                    
                        <div class="checkbox" id="change-password-checkbox">
                            <label>
                              <input type="checkbox" name="isChangePassword"> Изменить пароль
                            </label>
                          </div>
                    
                        <div class="form-group">
                            <label for="staff-form-password">Пароль</label>
                            <input type="password" class="form-control" id="staff-form-password" name="password">
                        </div>
                    
                        <div class="form-group">
                            <label for="staff-form-institutions">Заведения</label>
                            <select class="form-control" id="institutions" name="institutions[]" multiple="multiple">
                                @foreach ($institutions as $institution)
                                <option value="{{ $institution['id'] }}">{{ $institution['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <input type="hidden" name="staffId" id="staff-id">

                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="button" class="btn btn-warning btn-block" id="create-staff-save" data-loading-text="Сохранение...">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection