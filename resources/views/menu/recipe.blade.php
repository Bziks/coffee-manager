@extends('dashboard.index')

@section('dashboard.content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/js/DataTables/datatables.min.js') }}"></script>
    
    <div class="text-right">
        <button class="btn btn-warning" data-toggle="modal" data-target="#create-recipe-modal">Создать блюдо</button>
    </div>
    <br>
    
    @if (session('error'))
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            <li>{{ session('error') }}</li>
        </ul>
    </div>
    @endif
  
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <table id="recipe-table" class="table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Наименование</th>
                <th>Тип</th>
                <th>Количество</th>
                <th>Цена</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Наименование</th>
                <th>Тип</th>
                <th>Количество</th>
                <th>Цена</th>
                <th></th>
            </tr>
        </tfoot>
    </table>

    <script type="text/javascript" src="{{ asset("/js/recipe.js") }}"></script>
    <script>
    $(function () {
        recipe.initTable();
    });
    </script>
    
    <!-- Modals -->
    <!-- Создание/редактирование блюда -->
    <div class="modal fade" id="create-recipe-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3"><span class="recipe-title-modal"></span> блюда</h4>
                </div>
                <div class="modal-body">
                    <ul class="list-unstyled text-danger bg-danger error-list"></ul>

                    {!! Form::open(array('url' => 'menu/recipeCreate', 'files' => true)) !!}

                        <div class="form-group">
                            <label for="recipe-form-name">Название</label>
                            <input type="text" class="form-control" id="recipe-form-name" name="name" placeholder="Maguro Nigiri">
                        </div>
                        <div class="form-group">
                            <label for="recipe-form-picture">Картинка</label>
                            <input type="file" class="form-control" id="recipe-form-picture" name="picture">
                        </div>
                        <div class="form-group">
                            <label for="recipe-form-description">Описание</label>
                            <textarea class="form-control" id="recipe-form-description" name="description" rows="4" placeholder="Состав: тунец"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipe-form-type">Тип</label>
                            <input type="text" class="form-control" id="recipe-form-type" name="weight_type" placeholder="шт, г, л, мл, порции...">
                        </div>
                        <div class="form-group">
                            <label for="recipe-form-count">Количество</label>
                            <input type="number" class="form-control" id="recipe-form-count" name="weight_count" placeholder="6">
                        </div>
                        <div class="form-group">
                            <label for="recipe-form-price">Цена</label>
                            <input type="number" class="form-control" id="recipe-form-price" name="price" placeholder="1.70">
                        </div>

                        <input type="hidden" name="recipeId" id="recipe-id">

                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="button" class="btn btn-warning btn-block" id="create-recipe-save" data-loading-text="Сохранение...">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection