@extends('dashboard.index')

@section('dashboard.content')
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/js/DataTables/datatables.min.js') }}"></script>

    <div class="text-right">
        <button class="btn btn-warning" data-toggle="modal" data-target="#create-category-modal">Создать категорию</button>
    </div>
    <br>
    
    <table id="category-table" class="table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Наименование</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Наименование</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    
    <script type="text/javascript" src="{{ asset("/js/category.js") }}"></script>
    <script>
    $(function () {
        category.initTable();
    });
    </script>
    
    <!-- Modals -->
    <!-- Создание/редактирование категории -->
    <div class="modal fade" id="create-category-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3"><span class="category-title-modal"></span> блюда</h4>
                </div>
                <div class="modal-body">
                    <ul class="list-unstyled text-danger bg-danger error-list"></ul>

                    {!! Form::open(array('url' => 'menu/categoryCreate', 'files' => true)) !!}

                        <div class="form-group">
                            <label for="category-form-name">Название</label>
                            <input type="text" class="form-control" id="category-form-name" name="name" placeholder="Горячие блюда">
                        </div>
                        
                        <input type="hidden" name="categoryId" id="category-id">

                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="button" class="btn btn-warning btn-block" id="create-category-save" data-loading-text="Сохранение...">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection