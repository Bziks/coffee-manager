<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', '') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="/css/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">

        <!-- Scripts -->
        <script src="http://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="/js/bootstrap-fileinput/plugins/purify.min.js"></script>
        <script src="/js/bootstrap-fileinput/fileinput.min.js"></script>
        <script src="/js/bootstrap-fileinput/locales/ru.js"></script>
        
        <script src="{{ asset("/js/app.js") }}"></script>
        <script>
window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': window.Laravel.csrfToken
        }
    });
        </script>
    </head>
    <body>
        <div id="app">
            <nav class="menu-top navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', '') }}
                        </a>
                    </div>

                    <!-- Главная страница, регистрация, вход -->
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Вход</a></li>
                            <li><a href="{{ url('/register') }}">Регистрация</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/dashboard') }}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            Выход
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>

            @yield('content')

            <!-- footer -->
            <div class="container">
                <section style="height:80px;"></section>
                <div class="row" style="text-align:center;">
                </div>
                <!----------- Footer ------------>
                <footer class="footer-bs">
                    <div class="row">
                        <div class="col-md-3 footer-brand animated fadeInLeft">
                            <h2>Coffee manager</h2>                     
                            <p>© 2017, Все права защищены.</p>
                        </div>
                        <div class="col-md-4 footer-nav animated fadeInUp">
                            <h4>Меню —</h4>
                            <ul class="list">
                                <li><a href="{{ url('/about-us') }}">О нас</a></li>
                                <li><a href="{{ url('/contacts') }}">Контакты</a></li>
                                <li><a href="#">Правила и условия</a></li>
                                <li><a href="#">Политика конфидециальности</a></li>
                            </ul>
                        </div>
                        <div class="col-md-5 footer-social animated fadeInDown">
                            <h4>Добавьте нас</h4>
                            <ul>
                                <li><a href="https://vk.com/playful_joker" target="_blank">Вконтакте</a></li>
                                <li><a href="https://www.facebook.com/irina.naumova.50?fref=ts" target="_blank">Facebook</a></li>
                                <li><a href="https://twitter.com/irinanaumova951" target="_blank">Twitter</a></li>
                                <li><a href="https://www.instagram.com/beetle_fox/" target="_blank">Instagram</a></li>                           

                            </ul>
                        </div>
                    </div>
                </footer>
                <section style="text-align:center; margin:10px auto;"><p>Дизайн от <a href="#">Irina Naumova</a></p></section>

            </div>
        </div>

        <!-- Scripts -->
        <script src="/js/app.js"></script>
    </body>
</html>
