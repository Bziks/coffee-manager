@extends('dashboard.index')

@section('dashboard.content')
<!-- Подключение плагина DataTable -->
<link rel="stylesheet" type="text/css" href="{{ asset('/js/DataTables/datatables.min.css') }}"/>
<script type="text/javascript" src="{{ asset('/js/DataTables/datatables.min.js') }}"></script>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        @if ($success == true)
        <div class="text-center">
            <p class="bg-success text-success">Ваше заведение успешно 
                @if ($action == 'add')
                добавлено!
                @else
                сохранено!
                @endif
            </p>
            @if ($action == 'edit')
            <a href="{{ Request::url() }}" class="btn btn-warning">Назад к заведению</a>
            @endif
            <a href="{{ url('/institution') }}" class="btn btn-primary">К списку заведений</a>
        </div>
        @else

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="institution-tabs">
            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Информация</a></li>
            @if ($action == 'edit')
            <li role="presentation"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Меню</a></li>
            @endif
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="info">
                <br>
                
                @if ($action == 'add')
                {!! Form::open(array('url' => 'institution/add', 'files' => true)) !!}
                @else
                {!! Form::open(array('url' => 'institution/edit/'.$model['id'], 'files' => true)) !!}
                @endif

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    <label for="inst-name">Название</label>
                    <input type="text" class="form-control" name="name" id="inst-name" value='{{ $model['name'] }}'>
                    @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
                </div>
                <div class="form-group">
                    <label for="inst-address">Адрес</label>
                    <input type="text" class="form-control" name="address" id="inst-address" value='{{ $model['address'] }}'>
                    @if ($errors->has('address')) <p class="text-danger">{{ $errors->first('address') }}</p> @endif
                </div>
                <div class="form-group">
                    <label for="inst-description">Описание</label>
                    <textarea class="form-control" name="description" id="inst-description" rows="5">{{ $model['description'] }}</textarea>
                    @if ($errors->has('description')) <p class="text-danger">{{ $errors->first('description') }}</p> @endif
                </div>
                <div class="form-group">
                    <label for="inst-name">Картинка</label>
                    <input type="file" class="form-control" name="picture" id="inst-picture">
                    @if ($action == 'edit')
                    <input type="hidden" name="pictureIsChange" id="picture-is-change" value="false">
                    @endif
                </div>
                <div class="form-group">
                    <label for="inst-type">Тип</label>
                    <select name="type" class="form-control" id="inst-type">
                        @foreach ($institutionTypes as $type)
                        <option {{ $model['type'] == $type['id'] ? 'selected' : '' }} value="{{ $type['id'] }}">{{ $type['name'] }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="divider"></div>

                <h3>Особенности</h3>

                <div>
                    @foreach ($serviceTags as $index => $tag)
                    <label class="checkbox-inline">
                        <input type="checkbox" name="serviceTag[]" value="{{ $tag['id'] }}" {{ !empty($modelServiceTags[$tag['id']]) ? 'checked' : '' }}> {{ $tag['name'] }}
                    </label>
                    @if ($index != 0 && $index % 2 == 0)
                    <br>
                    @endif
                    @endforeach
                </div>

                <br>

                <div class="text-center">
                    @if ($action == 'add')
                    <button class="btn btn-warning">Создать</button>
                    @else
                    <button class="btn btn-warning">Сохранить</button>
                    @endif
                </div>

                {!! Form::close() !!}
            </div>
            @if ($action == 'edit')
            <div role="tabpanel" class="tab-pane" id="menu">
                <!-- Подключение модуля меню и его инициализация -->
                <script type="text/javascript" src="{{ asset("/js/menu.institution.js") }}"></script>
                <script>
                $(function () {
                    menuInstitution.initTabs();
                    menuInstitution.initImage("{{ $model["picture"] ? url("/").'/upload/institutions/'.$model["picture"].'?'.$model["pictureUpdate"] : '' }}");
                    menuInstitution.initTable({{ $model['id'] }});
                });
                </script>
                
                <br>

                <table class="table table-bordered" id="category-table">
                    <thead>
                        <tr>
                            <th>Наименование</th>
                            <th>Позиция</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

                <br>

                <div class="panel-group" id="category-accordion" role="tablist" aria-multiselectable="true">
                    @if ($institutionCategories)
                    @foreach ($institutionCategories as $index => $cat)
                    <div class="panel panel-default" id="panel-category-{{ $cat->id }}">
                        <div class="panel-heading {{ session('categoryId') && session('categoryId') == $cat->id ? '' : 'collapsed' }}" role="tab" id="heading{{ $index }}" data-toggle="collapse" data-parent="#category-accordion" href="#collapse{{ $index }}" aria-expanded="false" aria-controls="collapse{{ $index }}">
                            <h4 class="panel-title">
                                {{ $cat->name }}
                            </h4>
                        </div>
                        <div id="collapse{{ $index }}" class="panel-collapse collapse {{ session('categoryId') && session('categoryId') == $cat->id ? 'in' : '' }}" role="tabpanel" aria-labelledby="heading{{ $index }}">
                            <div class="panel-body">
                                <table class="table table-bordered recipe-table" data-category-id="{{ $cat->id }}">
                                    <thead>
                                        <tr>
                                            <th>Наименование</th>
                                            <th>Тип</th>
                                            <th>Количество</th>
                                            <th>Цена</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>

                <!-- Modals -->
                <!-- Добавление категории -->
                <div id="add-category-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="add-categoryModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="add-categoryModalLabel">Добавление категории</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-9">
                                        <select class="form-control" id="category-list">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-warning btn-block" id="add-category" data-loading-text="Сохранение...">Добавить</button>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-10">
                                        <h4 class="text-center"><small>- или создать новую категорию -</small></h4>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="create-category-name">
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-warning btn-block" id="create-category" data-loading-text="Сохранение...">Создать</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Редактирование позиции категории -->
                <div class="modal fade" id="category-position-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel1">Изменить позицию категории</h4>
                            </div>
                            <div class="modal-body">
                                <input type="number" class="form-control" id="category-position">
                                <input type="hidden" id="category-id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" class="btn btn-primary" id="category-position-save" data-loading-text="Сохранение...">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Добавление блюда -->
                <div class="modal fade" id="add-recipe-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel2">Добавление блюда</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-9">
                                        <select class="form-control" id="recipe-list">
                                            @foreach ($recipes as $recipe)
                                                <option value="{{ $recipe->id }}">{{ $recipe->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-warning btn-block" id="add-recipe" data-loading-text="Сохранение...">Добавить</button>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center"><small>- или -</small></h4>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button class="btn btn-warning btn-block" id="create-recipe-btn">Создать новое блюдо</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Создание блюда -->
                <div class="modal fade" id="create-recipe-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel3">Создание блюда</h4>
                            </div>
                            <div class="modal-body">
                                <ul class="list-unstyled text-danger bg-danger error-list"></ul>
                                
                                {!! Form::open(array('url' => 'institution/recipeCreate', 'files' => true)) !!}
                                
                                    <div class="form-group">
                                        <label for="recipe-form-name">Название</label>
                                        <input type="text" class="form-control" id="recipe-form-name" name="name" placeholder="Maguro Nigiri">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe-form-picture">Картинка</label>
                                        <input type="file" class="form-control" id="recipe-form-picture" name="picture">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe-form-description">Описание</label>
                                        <textarea class="form-control" id="recipe-form-description" name="description" rows="4" placeholder="Состав: тунец"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe-form-type">Тип</label>
                                        <input type="text" class="form-control" id="recipe-form-type" name="weight_type" placeholder="шт, г, л, мл, порции...">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe-form-count">Количество</label>
                                        <input type="number" class="form-control" id="recipe-form-count" name="weight_count" placeholder="6">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe-form-price">Цена</label>
                                        <input type="number" class="form-control" id="recipe-form-price" name="price" placeholder="1.70">
                                    </div>
                                    
                                    <input type="hidden" name="institutionId">
                                    <input type="hidden" name="categoryId">
                                    
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <button type="button" class="btn btn-warning btn-block" id="create-recipe-save" data-loading-text="Сохранение...">Сохранить</button>
                                        </div>
                                    </div>
                                
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Редактирование позиции блюда -->
                <div class="modal fade" id="recipe-position-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel4">Изменить позицию блюда</h4>
                            </div>
                            <div class="modal-body">
                                <input type="number" class="form-control" id="recipe-position">
                                <input type="hidden" id="recipe-id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" class="btn btn-primary" id="recipe-position-save" data-loading-text="Сохранение...">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Modals -->
            </div>
            @endif
        </div>
        @endif
    </div>
</div>
@endsection