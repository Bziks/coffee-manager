@extends('dashboard.index')

@section('dashboard.content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($deleted == true)
                <div class="text-center">
                    <p class="bg-success text-success">Ваше заведение успешно удалено!</p>
                    <a href="{{ url('/institution') }}" class="btn btn-primary">К списку заведений</a>
                </div>
            @else
                {!! Form::open(array('url' => 'institution/remove/'.$model['id'])) !!}
                
                <div class="text-center">
                    <p>Вы уверены что хотиде удалить <strong>"{{$model['name']}}"</strong>?</p>
                    
                    <input type="submit" name="accept" value="Да" class="btn btn-danger">
                    <a href="{{ url('/institution') }}" class="btn btn-warning">Нет</a>
                </div>
                
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@endsection