@extends('dashboard.index')

@section('dashboard.content')
    <a href='{{ url('/institution/add') }}' class="btn btn-warning">Добавить заведение</a>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th>Название</th>
                <th>Адрес</th>
                <th class="col-md-1 text-center"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($institutions as $institution)
            <tr>
                <td>{{ $institution->name }}</td>
                <td>{{ $institution->address }}</td>
                <td>
                    <a href="{{ url('/institution/' . $institution->id) }}"><i class="fa fa-eye"></i></a>
                    <a href="{{ url('/institution/edit/' . $institution->id) }}"><i class="fa fa-pencil"></i></a>
                    <a href="{{ url('/institution/remove/' . $institution->id) }}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection