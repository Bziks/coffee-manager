@extends('dashboard.index')

@section('dashboard.content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <ul class="nav nav-tabs" role="tablist" id="institution-tabs">
                <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Информация</a></li>
                <li role="presentation"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Меню</a></li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="info">
                    <br>
                    
                    <div class="form-group">
                        <label for="inst-name">Название</label>
                        <p>{{ $model['name'] }}</p>
                    </div>
                    <div class="form-group">
                        <label for="inst-address">Адрес</label>
                        <p>{{ $model['address'] }}</p>
                    </div>
                    <div class="form-group">
                        <label for="inst-description">Описание</label>
                        <p>{!! $model['description'] !!}</p>
                    </div>
                    <div class="form-group">
                        <label for="inst-name">Картинка</label>
                        <br>
                        @if ($model['picture'])
                            <img src="/upload/institutions/{{ $model['picture'] }}" class="preview-img" />
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="inst-type">Тип</label>
                        <p>{{ $institutionType }}</p>
                    </div>

                    <div class="divider"></div>

                    <h3>Особенности</h3>

                    <div>
                        @foreach ($serviceTags as $tag)
                            <ul class="list-unstyled">
                                @if (!empty($modelServiceTags[$tag['id']]))
                                    <li>{{ $tag['name'] }}</li>
                                @endif
                            </ul>
                        @endforeach
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="menu">
                    <br>
                    
                    <div class="panel-group" id="category-accordion" role="tablist" aria-multiselectable="true">
                        @if ($institutionCategories)
                        @foreach ($institutionCategories as $index => $cat)
                        <div class="panel panel-default" id="panel-category-{{ $cat->id }}">
                            <div class="panel-heading" role="tab" id="heading{{ $index }}" data-toggle="collapse" data-parent="#category-accordion" href="#collapse{{ $index }}" aria-expanded="false" aria-controls="collapse{{ $index }}">
                                <h4 class="panel-title">
                                    {{ $cat->name }}
                                </h4>
                            </div>
                            <div id="collapse{{ $index }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $index }}">
                                
                                <div class="list-group">
                                    @if (isset($recipes[$cat->id]))
                                    @foreach ($recipes[$cat->id] as $recipe)
                                    <div class="list-group-item">
                                        <h4 class="list-group-item-heading">{{ $recipe->name }}</h4>
                                        <div class="list-group-item-text">
                                            <div class="recipe-container">
                                                <img src="{{ url("/").'/upload/recipes/'.$recipe->picture }}" class="img-thumbnail pull-left" />
                                                <div class="pull-left">
                                                    {{ $recipe->description }}<br>
                                                    Кол-во: {{ (float)$recipe->weight_count }} {{ $recipe->weight_type }}<br>
                                                    Цена: {{ $recipe->price }}<br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="list-group-item text-center">Категория пуста</div>
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection