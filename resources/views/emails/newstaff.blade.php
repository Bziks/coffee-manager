<html>
<head></head>
<body>
    <h1>Добро пожаловать!</h1>
    <p>Уважаемый(ая) {{ $name }}.</p>
    <p>Вас добавили в систему Coffee Manager.</p>
    <p>Для входа Вам требуется перейти на <a href="http://{{ $domain }}/login" target="_blank">сайт</a>.</p>
    <p>Ввести ваш e-mail и пароль: <b>{{ $password }}</b></p>
</body>
</html>