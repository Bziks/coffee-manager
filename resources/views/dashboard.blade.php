@extends('dashboard.index')

@section('dashboard.content')
    @if ($countInstitutions > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Адрес</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($institutions as $institution)
                <tr>
                    <td>{{ $institution->name }}</td>
                    <td>{{ $institution->address }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>У вас нет заведений.</p>
        <a href="{{ url('/institution/add') }}" class="btn btn-warning">Создать заведение</a>
    @endif
@endsection
