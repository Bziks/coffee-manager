<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstitutionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        
        Schema::table('institutions', function($table) {
            $table->dropColumn('type');
        });
        
        Schema::table('institutions', function($table) {
            $table->bigInteger('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('institution_type');
    }
}
