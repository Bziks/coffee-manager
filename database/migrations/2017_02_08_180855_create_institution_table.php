<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_institution', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->bigInteger('institution_id');
        });
        Schema::create('institutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('address');
            $table->string('picture');
            $table->text('description');
            $table->enum('type', ['Ресторан', 'Кафе', 'Фаст-фуд']);
        });
        Schema::create('service_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('institution_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tag_id');
            $table->bigInteger('institution_id');
        });
        Schema::create('institutuin_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('institution_id');
            $table->bigInteger('category_id');
            $table->bigInteger('recipe_id');
        });
        Schema::create('category_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('picture');
            $table->string('description');
            $table->enum('weight_type', ['кг', 'л', 'г', 'мл', 'порции', 'шт']);
            $table->decimal('weight_count', 5, 4);
            $table->decimal('price', 5, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_institution');
        Schema::drop('institutions');
        Schema::drop('service_tags');
        Schema::drop('institution_tags');
        Schema::drop('institutuin_menu');
        Schema::drop('category_menu');
        Schema::drop('recipes');
    }
}
