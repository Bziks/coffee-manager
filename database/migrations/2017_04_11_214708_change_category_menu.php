<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCategoryMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_menu', function($table) {
            $table->integer('user_id')->after('id')->unsigned();
            $table->index('user_id', 'category_menu_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_menu', function($table) {
            $table->dropColumn('user_id');
            $table->dropIndex('category_menu_user_id');
        });
    }
}
