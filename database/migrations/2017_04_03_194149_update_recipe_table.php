<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function($table) {
            $table->integer('user_id')->after('id')->unsigned();
            $table->index('user_id', 'user_id');
            $table->decimal('weight_count', 9, 4)->change();
            $table->decimal('price', 9, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function($table) {
            $table->dropColumn('user_id');
            $table->dropIndex('user_id');
            $table->decimal('weight_count', 5, 4)->change();
            $table->decimal('price', 5, 2)->change();
        });
    }
}
