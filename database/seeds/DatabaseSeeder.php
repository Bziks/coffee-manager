<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        /*factory(App\Models\Recipe::class, 50)->create([
            'user_id' => 1
        ]);*/
        
        // Добавляет стандартные "Особенности" заведений
        DB::table('service_tags')->delete();
        DB::table('service_tags')->insert(['name' => 'Wi-Fi']);
        DB::table('service_tags')->insert(['name' => 'Парковка']);
        DB::table('service_tags')->insert(['name' => 'Столики на улице']);
        DB::table('service_tags')->insert(['name' => 'Зона для курения']);
        DB::table('service_tags')->insert(['name' => 'Cork fee']);
        DB::table('service_tags')->insert(['name' => 'Dj']);
        DB::table('service_tags')->insert(['name' => 'VIP зал']);
        DB::table('service_tags')->insert(['name' => 'Бесплатная вода от заведения']);
        DB::table('service_tags')->insert(['name' => 'Бизнес ланч']);
        DB::table('service_tags')->insert(['name' => 'Бильярд']);
        DB::table('service_tags')->insert(['name' => 'Детская комната']);
        DB::table('service_tags')->insert(['name' => 'Детский стульчик']);
        DB::table('service_tags')->insert(['name' => 'Детское меню']);
        DB::table('service_tags')->insert(['name' => 'Живая музыка']);
        DB::table('service_tags')->insert(['name' => 'Завтраки ']);
        DB::table('service_tags')->insert(['name' => 'Завтраки весь день']);
        DB::table('service_tags')->insert(['name' => 'Зал для курящих']);
        DB::table('service_tags')->insert(['name' => 'Кальян']);
        DB::table('service_tags')->insert(['name' => 'Круглосуточный']);
        DB::table('service_tags')->insert(['name' => 'Летняя терраса']);
        DB::table('service_tags')->insert(['name' => 'На воде']);
        DB::table('service_tags')->insert(['name' => 'На крыше']);
        DB::table('service_tags')->insert(['name' => 'Настольные игры заведения']);
        DB::table('service_tags')->insert(['name' => 'Настольные игры клиента']);
        DB::table('service_tags')->insert(['name' => 'Оплата кредитной карточкой']);
        DB::table('service_tags')->insert(['name' => 'Открытая кухня']);
        DB::table('service_tags')->insert(['name' => 'Плата за обслуживавние включена в чек']);
        DB::table('service_tags')->insert(['name' => 'С животными']);
        DB::table('service_tags')->insert(['name' => 'Танцпол']);
        DB::table('service_tags')->insert(['name' => 'Этно ресторан']);

        // Добавляет стандартные типы заведений
        DB::table('institution_type')->delete();
        DB::table('institution_type')->insert(['name' => 'Кафе']);
        DB::table('institution_type')->insert(['name' => 'Фаст-фуд']);
        DB::table('institution_type')->insert(['name' => 'Суши-бар']);
        DB::table('institution_type')->insert(['name' => 'Банкетный зал']);
        DB::table('institution_type')->insert(['name' => 'Бар']);
        DB::table('institution_type')->insert(['name' => 'Винотека']);
        DB::table('institution_type')->insert(['name' => 'Гостинично-ресторанный комплекс']);
        DB::table('institution_type')->insert(['name' => 'Караоке-клуб']);
        DB::table('institution_type')->insert(['name' => 'Кондитерская']);
        DB::table('institution_type')->insert(['name' => 'Кофейня']);
        DB::table('institution_type')->insert(['name' => 'Лаунж']);
        DB::table('institution_type')->insert(['name' => 'Ночной клуб']);
        DB::table('institution_type')->insert(['name' => 'Паб']);
        DB::table('institution_type')->insert(['name' => 'Пиццерия']);
        DB::table('institution_type')->insert(['name' => 'Пляжный комплекс']);
        DB::table('institution_type')->insert(['name' => 'Развлекательный комплекс']);
        DB::table('institution_type')->insert(['name' => 'Ресторан']);
        DB::table('institution_type')->insert(['name' => 'Траттория']);

    }
}
