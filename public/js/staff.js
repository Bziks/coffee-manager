// Модуль меню для заведения
// О модульной системе для javascript можно почитать тут 
// https://learn.javascript.ru/closures-module
// 
// О структуре именно текущего типа модуля можно почитать тут
// https://learn.javascript.ru/closures-module#экспорт-через-return
var staff = (function() {
    
    // Объект который будет публичным, т.е. доступным для вызова функций, которые будут в нем (функции в самом низу возле return).
    var scope = {};
    
    // Инициализирует плагин DataTable и дополнительный функционал
    // https://datatables.net/
    function initTable() {
        var staffTable = $('#staff-table').DataTable({
            "language": {
                "url": "/js/DataTables/lang.json"
            },
            "autoWidth": false, // Отключить автоматическую ширину таблицы (иначе она слишком узкая, не расстягивается на всю ширину страницы)
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/staff/staffAjax",
                "type": "POST"
            },
            "columns": [
                {"data": "name"},
                {"data": "email"},
                {"data": "institutions"},
                {
                    "className": 'remove-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<button class="btn btn-warning btn-xs staff-edit"><i class="fa fa-pencil"></i></button> <button class="btn btn-danger btn-xs staff-remove"><i class="fa fa-trash"></i></button>'
                }
            ]
        });
        
        initModalStaffBtns();
        
        // Обработчик удаления сотрудника
        $('#staff-table tbody').on('click', 'td.remove-control .staff-remove', function () {
            if (confirm("Подтверждаете удаление?")) {
                // Заблокировать кнопку
                var $btn = $(this).prop('disabled', 'disabled');
                // Узнать на какую строку нажали
                var tr = $(this).closest('tr');
                var row = staffTable.row(tr);

                $.ajax({
                    url: "/staff/staffRemoveAjax",
                    type: "post",
                    dataType: "json",
                    data: {
                        id: row.data().id
                    },
                    success: function(resp) {
                        if (resp.success) {
                            // Удалить строку и перезагрузить таблицу
                            row.remove().draw();
                        } else {
                            // Разблокировать кнопку
                            $btn.prop('disabled', '');
                            // Отобразить ошибку
                            alert(resp.error);
                        }
                    }
                });
            }
        });
        
        // Обработчки редактирования сотрудника
        $('#staff-table tbody').on('click', 'td.remove-control .staff-edit', function () {
            // Изменить заголовок окна
            setTitleModal('Редактирование');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = staffTable.row(tr);
            var data = row.data();
            
            $('#create-staff-modal #change-password-checkbox').css('display', 'block');
            $('#create-staff-modal #staff-form-password').attr('readonly', 'readonly');
            
            // Подставить данные блюда в поля формы
            $('#create-staff-modal #staff-id').val(data.id);
            $('#create-staff-modal #staff-form-name').val(data.name);
            $('#create-staff-modal #staff-form-email').val(data.email);
            $('#create-staff-modal #staff-form-password').val("some password");
            $('#create-staff-modal #institutions').val(data.institutions_id);
            
            // Открыть модальное окно с интерфейсом изменения позиции
            $('#create-staff-modal').modal('show');
        });
    }
    
    // Функционал модальных окон для сотрудников
    function initModalStaffBtns() {
        // Изменить заголовок окна
        setTitleModal('Создание');
        
        // Когда окно закрывается, то выполнить код что в функции
        $('#create-staff-modal').on('hidden.bs.modal', function(){
            // Изменить заголовок окна
            setTitleModal('Создание');
            // Очистить форму
            $('#create-staff-modal form')[0].reset();
            // Очистить id сотрудника
            $('#create-staff-modal #staff-id').val('');
            
            // Скрыть чекбокс "Изменить пароль"
            $('#create-staff-modal #change-password-checkbox').css('display', 'none');
            // Разблокировать поле "Пароль"
            $('#create-staff-modal #staff-form-password').removeAttr('readonly');
        });
        
        // Обработчик нажатия на чекбокс "Изменить пароль"
        $('#create-staff-modal #change-password-checkbox input[type="checkbox"]').click(function(){
            if ($(this).prop('checked')) { // Если стоит галочка, то разблокировать поле пароля
                $('#create-staff-modal #staff-form-password').removeAttr('readonly');
                $('#create-staff-modal #staff-form-password').val('');
            } else { // иначе заблокировать поле пароля
                $('#create-staff-modal #staff-form-password').attr('readonly', 'readonly');
                $('#create-staff-modal #staff-form-password').val('some password');
            }
        });
        
        // Сохранение сотрудника
        $('#create-staff-save').click(function(){
            // Блокировка кнопки "Сохранить"
            var $btn = $(this).button('loading');
            // Скрыть ошибки
            $('#create-staff-modal .error-list').css('display', 'none');
            
            // Список ошибок
            var error = "";
            // Пройтись по всем полям для ввода и проверить заполнены ли они
            $('#create-staff-modal form .form-control').each(function(){
                // Если поле не div и оно пустое, то добавить в переменную ошибок
                if ($(this).val() == "") {
                    error += "<li>Заполните поле "+ $(this).closest('.form-group').find('label').text() +"</li>";
                }
            });
            
            if (error != "") { // Если есть ошибки
                // Отобразить блок с ошибками
                $('#create-staff-modal .error-list').css('display', 'block');
                // Заполнить блок ошибками
                $('#create-staff-modal .error-list').html(error);
                // Разблокироать кнопку
                $btn.button('reset');
                //Прокрутить скрол к полю с ошибками
                $('#create-staff-modal .error-list')[0].scrollIntoView(true);
            } else { // Если ошибок нет
                // Програмно отправить форму.
                // Нужно для того что бы корректно передать картинку на сервер
                // Можно было и с помощью ajax, но пришлось бы заморачиваться с проверками
                $('#create-staff-modal form')[0].submit();
            }
        });
    }
    
    // Изменяет заголовок модального окна
    function setTitleModal(title) {
        $('.staff-title-modal').text(title);
    }
    
    // Присвоим функции, которые нужно вынести из модуля
    scope.initTable = initTable;

    return scope;
})();