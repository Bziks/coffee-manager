// Модуль меню для заведения
// О модульной системе для javascript можно почитать тут 
// https://learn.javascript.ru/closures-module
// 
// О структуре именно текущего типа модуля можно почитать тут
// https://learn.javascript.ru/closures-module#экспорт-через-return
var category = (function() {
    
    // Объект который будет публичным, т.е. доступным для вызова функций, которые будут в нем (функции в самом низу возле return).
    var scope = {};
    
    // Инициализирует плагин DataTable и дополнительный функционал
    // https://datatables.net/
    function initTable() {
        var categoryTable = $('#category-table').DataTable({
            "language": {
                "url": "/js/DataTables/lang.json"
            },
            "autoWidth": false, // Отключить автоматическую ширину таблицы (иначе она слишком узкая, не расстягивается на всю ширину страницы)
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/menu/categoryAjax",
                "type": "POST"
            },
            "columns": [
                {"data": "name"},
                {
                    "className": 'remove-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<button class="btn btn-warning btn-xs category-edit"><i class="fa fa-pencil"></i></button> <button class="btn btn-danger btn-xs category-remove"><i class="fa fa-trash"></i></button>'
                }
            ]
        });
        
        initModalCategoryBtns();
        
        // Обработчик удаления блюда
        $('#category-table tbody').on('click', 'td.remove-control .category-remove', function () {
            if (confirm("Подтверждаете удаление?")) {
                // Заблокировать кнопку
                var $btn = $(this).prop('disabled', 'disabled');
                // Узнать на какую строку нажали
                var tr = $(this).closest('tr');
                var row = categoryTable.row(tr);

                $.ajax({
                    url: "/menu/categoryRemoveAjax",
                    type: "post",
                    dataType: "json",
                    data: {
                        id: row.data().id
                    },
                    success: function(resp) {
                        if (resp.success) {
                            // Удалить строку и перезагрузить таблицу
                            row.remove().draw();
                        } else {
                            // Разблокировать кнопку
                            $btn.prop('disabled', '');
                            // Отобразить ошибку
                            alert(resp.error);
                        }
                    }
                });
            }
        });
        
        // Обработчки редактирования блюда
        $('#category-table tbody').on('click', 'td.remove-control .category-edit', function () {
            // Изменить заголовок окна
            setTitleModal('Редактирование');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = categoryTable.row(tr);
            var data = row.data();
            
            // Подставить данные блюда в поля формы
            $('#create-category-modal #category-id').val(data.id);
            $('#create-category-modal #category-form-name').val(data.name);
            
            // Открыть модальное окно с интерфейсом изменения позиции
            $('#create-category-modal').modal('show');
        });
    }
    
    // Функционал модальных окон для категорий
    function initModalCategoryBtns() {
        // Изменить заголовок окна
        setTitleModal('Создание');
        
        // Когда окно закрывается, то выполнить код что в функции
        $('#create-category-modal').on('hidden.bs.modal', function(){
            // Изменить заголовок окна
            setTitleModal('Создание');
            // Очистить форму
            $('#create-category-modal form')[0].reset();
            // Очистить id категории
            $('#create-category-modal #category-id').val('');
        });
        
        // Сохранение нового блюда
        $('#create-category-save').click(function(){
            // Блокировка кнопки "Сохранить"
            var $btn = $(this).button('loading');
            // Скрыть ошибки
            $('#create-category-modal .error-list').css('display', 'none');
            
            // Список ошибок
            var error = "";
            // Если поле "Название" пустое, то добавить в переменную ошибок
            if ($('#create-category-modal #category-form-name').val() == "") {
                error += "<li>Заполните поле \"Название\"</li>";
            }
            
            if (error != "") { // Если есть ошибки
                // Отобразить блок с ошибками
                $('#create-category-modal .error-list').css('display', 'block');
                // Заполнить блок ошибками
                $('#create-category-modal .error-list').html(error);
                // Разблокироать кнопку
                $btn.button('reset');
                //Прокрутить скрол к полю с ошибками
                $('#create-category-modal .error-list')[0].scrollIntoView(true);
            } else { // Если ошибок нет
                // Програмно отправить форму.
                // Нужно для того что бы корректно передать картинку на сервер
                // Можно было и с помощью ajax, но пришлось бы заморачиваться с проверками
                $('#create-category-modal form')[0].submit();
            }
        });
    }
    
    // Изменяет заголовок модального окна
    function setTitleModal(title) {
        $('.category-title-modal').text(title);
    }
    
    // Присвоим функции, которые нужно вынести из модуля
    scope.initTable = initTable;

    return scope;
})();