// Модуль меню для заведения
// О модульной системе для javascript можно почитать тут 
// https://learn.javascript.ru/closures-module
// 
// О структуре именно текущего типа модуля можно почитать тут
// https://learn.javascript.ru/closures-module#экспорт-через-return
var recipe = (function() {
    
    // Объект который будет публичным, т.е. доступным для вызова функций, которые будут в нем (функции в самом низу возле return).
    var scope = {};
    
    // Инициализирует плагин DataTable и дополнительный функционал
    // https://datatables.net/
    function initTable() {
        var recipeTable = $('#recipe-table').DataTable({
            "language": {
                "url": "/js/DataTables/lang.json"
            },
            "autoWidth": false, // Отключить автоматическую ширину таблицы (иначе она слишком узкая, не расстягивается на всю ширину страницы)
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/menu/recipeAjax",
                "type": "POST"
            },
            "columns": [
                {"data": "name"},
                {"data": "weight_type"},
                {"data": "weight_count"},
                {"data": "price"},
                {
                    "className": 'remove-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<button class="btn btn-warning btn-xs recipe-edit"><i class="fa fa-pencil"></i></button> <button class="btn btn-danger btn-xs recipe-remove"><i class="fa fa-trash"></i></button>'
                }
            ]
        });
        
        initModalRecipeBtns();
        
        // Обработчик удаления блюда
        $('#recipe-table tbody').on('click', 'td.remove-control .recipe-remove', function () {
            if (confirm("Подтверждаете удаление?")) {
                // Заблокировать кнопку
                var $btn = $(this).prop('disabled', 'disabled');
                // Узнать на какую строку нажали
                var tr = $(this).closest('tr');
                var row = recipeTable.row(tr);

                $.ajax({
                    url: "/menu/recipeRemoveAjax",
                    type: "post",
                    dataType: "json",
                    data: {
                        id: row.data().id
                    },
                    success: function(resp) {
                        if (resp.success) {
                            // Удалить строку и перезагрузить таблицу
                            row.remove().draw();
                        } else {
                            // Разблокировать кнопку
                            $btn.prop('disabled', '');
                            // Отобразить ошибку
                            alert(resp.error);
                        }
                    }
                });
            }
        });
        
        // Обработчки редактирования блюда
        $('#recipe-table tbody').on('click', 'td.remove-control .recipe-edit', function () {
            // Изменить заголовок окна
            setTitleModal('Редактирование');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = recipeTable.row(tr);
            var data = row.data();
            
            // Подставить данные блюда в поля формы
            $('#create-recipe-modal #recipe-id').val(data.id);
            $('#create-recipe-modal #recipe-form-name').val(data.name);
            $('#create-recipe-modal #recipe-form-picture').fileinput('refresh', {
                initialPreview: ['/upload/recipes/' + data.picture],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {width: "120px", showDelete: false}
                ],
                overwriteInitial: true
            });
            $('#create-recipe-modal #recipe-form-description').val(data.description);
            $('#create-recipe-modal #recipe-form-type').val(data.weight_type);
            $('#create-recipe-modal #recipe-form-count').val(data.weight_count);
            $('#create-recipe-modal #recipe-form-price').val(data.price);
            
            // Открыть модальное окно с интерфейсом изменения позиции
            $('#create-recipe-modal').modal('show');
        });
    }
    
    // Функционал модальных окон для блюд
    function initModalRecipeBtns() {
        // Оформляем красиво загрузчик картинки блюда
        $('#recipe-form-picture').fileinput({
            allowedFileTypes: ['image'],
            showRemove: false,
            showUpload: false,
            browseLabel: 'Выбрать...'
        });
        
        // Изменить заголовок окна
        setTitleModal('Создание');
        
        // Когда окно закрывается, то выполнить код что в функции
        $('#create-recipe-modal').on('hidden.bs.modal', function(){
            // Изменить заголовок окна
            setTitleModal('Создание');
            // Очистить форму
            $('#create-recipe-modal form')[0].reset();
            // Очистить id блюда
            $('#create-recipe-modal #recipe-id').val('');
            // Очистить картинку
            $('#recipe-form-picture').fileinput('clear');
        });
        
        // Сохранение нового блюда
        $('#create-recipe-save').click(function(){
            // Блокировка кнопки "Сохранить"
            var $btn = $(this).button('loading');
            // Скрыть ошибки
            $('#create-recipe-modal .error-list').css('display', 'none');
            
            // Список ошибок
            var error = "";
            // Пройтись по всем полям для ввода и проверить заполнены ли они
            $('#create-recipe-modal form .form-control').each(function(){
                // Если поле не div и оно пустое, то добавить в переменную ошибок
                if (($(this)[0].tagName != "DIV" && $(this).val() == "")) {
                    if (($('#create-recipe-modal #recipe-id').val() == "" && $(this).attr('id') == "recipe-form-picture")) {
                        error += "<li>Заполните поле "+ $(this).closest('.form-group').find('label').text() +"</li>";
                    }
                }
            });
            
            if (error != "") { // Если есть ошибки
                // Отобразить блок с ошибками
                $('#create-recipe-modal .error-list').css('display', 'block');
                // Заполнить блок ошибками
                $('#create-recipe-modal .error-list').html(error);
                // Разблокироать кнопку
                $btn.button('reset');
                //Прокрутить скрол к полю с ошибками
                $('#create-recipe-modal .error-list')[0].scrollIntoView(true);
            } else { // Если ошибок нет
                // Програмно отправить форму.
                // Нужно для того что бы корректно передать картинку на сервер
                // Можно было и с помощью ajax, но пришлось бы заморачиваться с проверками
                $('#create-recipe-modal form')[0].submit();
            }
        });
    }
    
    // Изменяет заголовок модального окна
    function setTitleModal(title) {
        $('.recipe-title-modal').text(title);
    }
    
    // Присвоим функции, которые нужно вынести из модуля
    scope.initTable = initTable;

    return scope;
})();