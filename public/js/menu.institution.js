// Модуль меню для заведения
// О модульной системе для javascript можно почитать тут 
// https://learn.javascript.ru/closures-module
// 
// О структуре именно текущего типа модуля можно почитать тут
// https://learn.javascript.ru/closures-module#экспорт-через-return
var menuInstitution = (function () {
    
    // Объект который будет публичным, т.е. доступным для вызова функций, которые будут в нем (функции в самом низу возле return).
    var scope = {};
    // id заведения, которое будут просматривать
    var institutionId = 0;
    // id категории, которую выбрали
    var categoryId = 0;
    
    function initTabs() {
        // Если в url адрессе есть хеш #menu, то переключаем на вкладку "Меню"
        // Пример url с хешем: http://coffee.loc/institution/edit/14#menu
        if (location.hash == "#menu") {
            $('#institution-tabs a[href="#menu"]').tab('show')
        }
    }
    
    // Преобразовываем картинку заведения в красивый интерфейс
    // Используется данный плагин:
    // http://plugins.krajee.com/file-input
    function initImage(initialUrl) {
        if (initialUrl != '') { // Если картинка уже есть, то отобразить её в превью
            initialUrl = initialUrl;
            $("#inst-picture").fileinput({
                initialPreview: [initialUrl],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {width: "120px", showDelete: false}
                ],
                overwriteInitial: true,
                allowedFileTypes: ['image'],
                showRemove: false,
                showUpload: false,
                browseLabel: 'Выбрать...'
            });
        } else { // Если картинки нет, то не отображать
            $("#inst-picture").fileinput({
            
                allowedFileTypes: ['image'],
                showRemove: false,
                showUpload: false,
                browseLabel: 'Выбрать...'
            });
        }
        
        // Если картинку поменяли, то указать это в форме
        $('#inst-picture').on('change', function(event) {
            $('#picture-is-change').val('true');
        });
    }
    
    // Инициализирует плагин DataTable и дополнительный функционал
    // https://datatables.net/
    function initTable(_institutionId) {
        // Запоминает id текущего заведения
        institutionId = _institutionId;
        initCategoryTable();
        initRecipeTable();
    }
    
    // Функционал для таблицы категорий
    function initCategoryTable() {
        var categoryTable = $('#category-table').on( 'init.dt', function () { // Ждем когда плагин DataTable загрузится
            // Добавить в таблицу кнопку "Добавить категорию"
            $("#category-table_wrapper .toolbar").html('<button class="btn btn-warning" data-toggle="modal" data-target="#add-category-modal">Добавить категорию</button>');
            // Инициализировать обработчики на другие кнопки
            initModalCategoryBtns();
        }).DataTable({
            "language": {
                "url": "/js/DataTables/lang.json" // Откуда подтянуть локализацию на русском языке
            },
            "autoWidth": false, // Отключить автоматическую ширину таблицы (иначе она слишком узкая, не расстягивается на всю ширину страницы)
            "dom": '<"toolbar">frtip', // Указать что подключаем toolbar вместо стандартного <select>
            "processing": true, // Включить индикатор загрузки данных
            "serverSide": true, // Включить подгрузку данных с сервера
            "ajax": {
                "url": "/institution/categoryAjax", // Откуда подтянуть данные для таблицы
                "type": "POST",
                data: {
                    // Дополнительный параметр, который подставится в запрос для получения данных в таблицу
                    institutionId: institutionId // id заведения
                },
            },
            "columns": [ // Список колонок в таблице
                {"data": "name"},
                {"data": "position"},
                {
                    "className": 'remove-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<button class="btn btn-warning btn-xs category-edit"><i class="fa fa-pencil"></i></button> <button class="btn btn-danger btn-xs category-remove"><i class="fa fa-trash"></i></button>'
                }
            ]
        });
        
        // Обработчик удаления категорий из таблицы
        $('#category-table tbody').on('click', 'td.remove-control .category-remove', function () {
            // Заблокировать кнопку
            var $btn = $(this).prop('disabled', 'disabled');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = categoryTable.row(tr);
            
            // Послать ajax запрос на удаление категории
            $.ajax({
                url: "/institution/categoryRemoveAjax",
                type: "post",
                data: {
                    institutionId: institutionId, // id заведения
                    id: row.data().id // id категории
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) { // Если удаление прошло успешно, то удаляем категорию и обновляем данные в таблице
                        $('#panel-category-' + row.data().id).remove();
                        row.remove().draw();
                    } else { // Если не успешно, то отобразить ошибку
                        // Разблокировать кнопку
                        $btn.prop('disabled', '');
                        alert(resp.error);
                    }
                }
            });
        });
        
        // Обработчки изменения позиции категории
        $('#category-table tbody').on('click', 'td.remove-control .category-edit', function () {
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = categoryTable.row(tr);
            // Узнать текущую позицию
            var position = row.data().position;
            
            //Подставить текущую позицию в поле для ввода новой позиции
            $('#category-position-modal #category-position').val(position);
            // Подставить id категории в скрытое поле формы
            $('#category-position-modal #category-id').val(row.data().id);
            // Открыть модальное окно с интерфейсом изменения позиции
            $('#category-position-modal').modal('show');
        });
        
        // Сохранить новую позицию
        $('#category-position-modal #category-position-save').click(function(){
            // Сделать кнопку неактивной, что бы не нажимали по несколько раз на неё
            var $btn = $(this).button('loading');
            
            // Отправить запрос на сохранение новой позиции
            $.ajax({
                url: "/institution/categoryEditAjax",
                type: "post",
                data: {
                    institutionId: institutionId,
                    categoryId: $('#category-id').val(),
                    position: $('#category-position').val()
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) { // Если всё успешно, то перезагрузить страницу, подставив хеш #menu в url
                        location.hash = '#menu';
                        location.reload();
                    } else { // Если не успешно, то разблокировать кнопку и вывести ошибку
                        $btn.button('reset');
                        alert(resp.error);
                    }
                }
            });
        });
    }
    
    // Функционал для модальных окон связаных с категориями
    function initModalCategoryBtns() {
        // Добавляет категорию из списка категорий в меню текущего заведения
        $('#add-category-modal #add-category').click(function(){
            var $btn = $(this).button('loading');
            
            $.ajax({
                url: "/institution/categoryAddAjax",
                type: "post",
                data: {
                    institutionId: institutionId, // id заведения
                    id: $('#category-list').val() // id категории
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) {
                        location.hash = '#menu';
                        location.reload();
                    } else {
                        $btn.button('reset');
                        alert(resp.error);
                    }
                }
            });
        });
        
        // Создает новую категорию и добавляет в меню текущего заведения
        $('#add-category-modal #create-category').click(function(){
            var $btn = $(this).button('loading');
            
            $.ajax({
                url: "/institution/categoryCreateAjax",
                type: "post",
                data: {
                    institutionId: institutionId, // id заведения
                    name: $('#create-category-name').val() // название новой категории
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) {
                        location.hash = '#menu';
                        location.reload();
                    } else {
                        $btn.button('reset');
                        alert(resp.error);
                    }
                }
            });
        });
    }
    
    // Функционал для таблицы блюд
    function initRecipeTable() {
        // Будет хранится та таблица блюд, в которой редактируют или удаляют блюдо
        var recipeTable = null;
        
        $('.recipe-table').on( 'init.dt', function () { // Ждем когда плагин DataTable загрузится
            // Добавить в таблицу кнопку "Добавить блюдо"
            $(this).closest('.dataTables_wrapper').find(".toolbar").html('<button class="btn btn-warning add-recipe-modal-btn">Добавить блюдо</button>');
            // Инициализировать обработчик кнопок
            initAddRecipeBtn();
        }).DataTable({
            "language": {
                "url": "/js/DataTables/lang.json"
            },
            "autoWidth": false,
            "dom": '<"toolbar">frtip',
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/institution/recipeAjax",
                "type": "POST",
                "data": function(d, dt) {
                    // id категории
                    d.categoryId = $(dt.nTable).attr('data-category-id');
                    // id заведения
                    d.institutionId = institutionId;
                }
            },
            "columns": [ // Колонки таблицы блюд
                {"data": "name"},
                {"data": "weight_type"},
                {"data": "weight_count"},
                {"data": "price"},
                {
                    "className": 'remove-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '<button class="btn btn-warning btn-xs recipe-edit"><i class="fa fa-pencil"></i></button> <button class="btn btn-danger btn-xs recipe-remove"><i class="fa fa-trash"></i></button>'
                }
            ]
        });
        
        // Функционал модальных окон связаных с таблицей блюд
        initModalRecipeBtns();

        // Удаление блюда
        $('.recipe-table tbody').on('click', 'td.remove-control .recipe-remove', function () {
            // Заблокировать кнопку
            var $btn = $(this).prop('disabled', 'disabled');
            // Выбрать текущую таблицу (таблиц с блюдом будет столько сколько и категорий,
            // поэтому нужно точно узнать в какой таблице нажали кнопку
            recipeTable = $(this).closest('.dataTables_wrapper').find('.recipe-table');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = recipeTable.DataTable().row(tr);

            $.ajax({
                url: "/institution/recipeRemoveAjax",
                type: "post",
                dataType: "json",
                data: {
                    institutionId: institutionId,
                    categoryId: recipeTable.attr('data-category-id'),
                    recipeId: row.data().id
                },
                success: function(resp) {
                    if (resp.success) {
                        row.remove().draw();
                    } else {
                        // Разблокировать кнопку
                        $btn.prop('disabled', '');
                        alert(resp.error);
                    }
                }
            });
        });
        
        // Обработчки изменения позиции блюда
        $('.recipe-table tbody').on('click', 'td.remove-control .recipe-edit', function () {
            // Выбрать текущую таблицу (таблиц с блюдом будет столько сколько и категорий,
            // поэтому нужно точно узнать в какой таблице нажали кнопку
            recipeTable = $(this).closest('.dataTables_wrapper').find('.recipe-table');
            // Узнать на какую строку нажали
            var tr = $(this).closest('tr');
            var row = recipeTable.DataTable().row(tr);
            // Узнать текущую позицию
            var position = row.data().position;
            
            //Подставить текущую позицию в поле для ввода новой позиции
            $('#recipe-position-modal #recipe-position').val(position);
            // Подставить id блюда в скрытое поле формы
            $('#recipe-position-modal #recipe-id').val(row.data().id);
            // Открыть модальное окно с интерфейсом изменения позиции
            $('#recipe-position-modal').modal('show');
        });
        
        // Сохранить новую позицию
        $('#recipe-position-modal #recipe-position-save').click(function(){
            // Сделать кнопку неактивной, что бы не нажимали по несколько раз на неё
            var $btn = $(this).button('loading');
            
            // Отправить запрос на сохранение новой позиции
            $.ajax({
                url: "/institution/recipeEditAjax",
                type: "post",
                data: {
                    institutionId: institutionId,
                    categoryId: recipeTable.attr('data-category-id'),
                    recipeId: $('#recipe-id').val(),
                    position: $('#recipe-position').val()
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) { // Если всё успешно, то обновить таблицу блюд
                        recipeTable.DataTable().draw();
                        $('#recipe-position-modal').modal('hide');
                    } else { // Если не успешно, то вывести ошибку
                        alert(resp.error);
                    }
                    
                    // Разблокировать кнопку 
                    $btn.button('reset');
                }
            });
        });
    }
    
    // Функционал кнопки "Добавить блюдо"
    function initAddRecipeBtn() {
        // Отключить данный функционал
        // Описание: Так как функция initAddRecipeBtn() вызывается столько раз сколько и категорий, то
        // если не отключать сначала данный функционал - он сработает за одно нажатие на кнопку
        // столько раз сколько и категорий. А нужно что бы сработал один раз. Поэтому сначала
        // нужно отключить (удалть из памяти js уже включенный функционал) и включить новый.
        $('.add-recipe-modal-btn').off('click');
        
        // Открытие окна в котором можно добавить блюдо
        $('.add-recipe-modal-btn').click(function(){
            // Запомнить в какой категории была нажата кнопка, что бы в дальнейшем в эту категорию добавить блюдо
            categoryId = $(this).closest('.dataTables_wrapper').find('.recipe-table').attr('data-category-id');
            $('#add-recipe-modal').modal('show');
        });
    }
    
    // Функционал модальных окон для блюд
    function initModalRecipeBtns() {
        // Переменная состояния, нужна для определения нажал ли пользователь
        // на кнопку "Создать блюдо".
        var isCreateRecipe = false;
        
        // Нажатие на кнопку "Создать блюдо"
        $('#create-recipe-btn').click(function(){
            // Изменить состояние
            isCreateRecipe = true;
            // Скрыть модальное окно "Добавление блюда"
            $('#add-recipe-modal').modal('hide');
        });
        
        // Обработчки скрытия модального окна "Добавление блюда"
        // Срабатывает КАЖДЫЙ раз как закрывают модальное окно
        $('#add-recipe-modal').on('hidden.bs.modal', function(){
            // Так как данный обработчик срабатывает КАЖДЫЙ раз при закрытии модального окна,
            // нужно определить что пользователь нажал на кнопку "Создать блюдо", а не просто
            // закрыл окно. Для этого и задействована переменная состояния isCreateRecipe
            if (isCreateRecipe) {
                // Открыть окно "Создание блюда"
                $('#create-recipe-modal').modal('show');
                // Изменить состояние что бы при следующих нажатиях на "Создать блюдо"
                // срабатывал данный код
                isCreateRecipe = false;
            }
        });
        
        // Оформляем красиво загрузчик картинки блюда
        $('#recipe-form-picture').fileinput({
            allowedFileTypes: ['image'],
            showRemove: false,
            showUpload: false,
            browseLabel: 'Выбрать...'
        });
        
        // Добавление блюда из списка существующих блюд
        $('#add-recipe').click(function(){
            var $btn = $(this).button('loading');
            
            $.ajax({
                url: "/institution/recipeAddAjax",
                type: "post",
                data: {
                    institutionId: institutionId, // id заведения
                    categoryId: categoryId, // id категории
                    recipeId: $('#recipe-list').val() // id блюда
                },
                dataType: 'json',
                success: function(resp) {
                    if (resp.success) { // Если успешно добавилось, то обновить таблицу с блюдами и скрыть модальное окно
                        $('.recipe-table[data-category-id="'+categoryId+'"]').DataTable().draw();
                        $('#add-recipe-modal').modal('hide');
                    } else { // Если неуспешно, то отобразить ошибку
                        alert(resp.error);
                    }
                    $btn.button('reset');
                }
            });
        });
        
        // Сохранение нового блюда
        $('#create-recipe-save').click(function(){
            // Блокировка кнопки "Сохранить"
            var $btn = $(this).button('loading');
            // Скрыть ошибки
            $('#create-recipe-modal .error-list').css('display', 'none');
            
            // Список ошибок
            var error = "";
            // Пройтись по всем полям для ввода и проверить заполнены ли они
            $('#create-recipe-modal form .form-control').each(function(){
                // Если поле не div и оно пустое, то добавить в переменную ошибок
                if ($(this)[0].tagName != "DIV" && $(this).val() == "") {
                    error += "<li>Заполните поле "+ $(this).closest('.form-group').find('label').text() +"</li>";
                }
            });
            
            if (error != "") { // Если есть ошибки
                // Отобразить блок с ошибками
                $('#create-recipe-modal .error-list').css('display', 'block');
                // Заполнить блок ошибками
                $('#create-recipe-modal .error-list').html(error);
                // Разблокироать кнопку
                $btn.button('reset');
                //Прокрутить скрол к полю с ошибками
                $('#create-recipe-modal .error-list')[0].scrollIntoView(true);
            } else { // Если ошибок нет
                // Заполнить скрытые поля id заведения и id категории
                $('#create-recipe-modal form [name=institutionId]').val(institutionId);
                $('#create-recipe-modal form [name=categoryId]').val(categoryId);
                // Програмно отправить форму.
                // Нужно для того что бы корректно передать картинку на сервер
                // Можно было и с помощью ajax, но пришлось бы заморачиваться с проверками
                $('#create-recipe-modal form')[0].submit();
            }
        });
    }

    // Присвоим функции, которые нужно вынести из модуля
    scope.initTabs = initTabs;
    scope.initTable = initTable;
    scope.initImage = initImage;

    return scope;

})();